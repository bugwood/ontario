//
//  AboutViewController.m
//  ontario
//
//  Created by Jordan Daniel on 9/24/15.
//  Copyright © 2015 com.bugwood. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIWebView *aboutWebView;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.aboutWebView setDelegate:self];
    [self.aboutWebView loadHTMLString:@"<p>EDDMapS Ontario brings the power of EDDMapS to your iOS device. Now you can submit invasive species observations directly with your iOS device from the field. These reports are uploaded to EDDMapS and e-mailed directly to local and state verifiers for review. EDDMapS Ontario was developed by the University of Georgia Center for Invasive Species and Ecosystem Health.</p><p>App developed by Jordan Daniel and Chuck Bargeron, University of Georgia – Center for Invasive Species and Ecosystem Health. <br><a href='http://apps.bugwood.org/'>http://apps.bugwood.org/</a></p><p>EDDMapS Ontario was developed through the support and funding provided by the Canada/Ontario Invasive Species Centre, the Ontario Federation of Anglers and Hunters, and the Ontario Ministry of Natural Resources.</p><p><b>Features:</b></p><p>Easy species reporting that captures your current location and allows you to submit an image of your sightings. EDDMapS Ontario allows for both online and offline reporting with reports saved on your phone for uploading when you have network connectivity.</p><p>Images and information on Ontario\'s worst non-native invasive animals and plants.</p><p>Real-time point distribution maps centered on your current location.</p><p>Powered by EDDMapS - The University of Georgia Center for Invasive Species and Ecosystem Health\'s Early Detection and Distribution Mapping System. EDDMapS allows for real time tracking of invasive species occurrences using local and national distribution maps and electronic early detection reporting tools. <br>For more information about EDDMapS, <br>visit <a href='http://www.eddmaps.org/'>http://www.eddmaps.org/</a></p>" baseURL:nil ];
    
    [self.versionLabel setText:[NSString stringWithFormat:@"Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [webView sizeToFit];
    
    for (NSLayoutConstraint *constraint in [webView constraints]) {
        [constraint setConstant:webView.scrollView.frame.size.height];
    }

}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Webview Error" message:[NSString stringWithFormat:@"Sorry but the web view was unalble to load - %@", error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
