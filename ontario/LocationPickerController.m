//
//  LocationPickerController.m
//  Squeal on Pigs
//
//  Created by Jordan Daniel on 5/15/13.
//  Copyright (c) 2013 Jordan Daniel. All rights reserved.
//

#import "LocationPickerController.h"
#import <GoogleMaps/GoogleMaps.h>

#define METERS_PER_MILE 1609.344

@interface LocationPickerController ()

@property (nonatomic) NSString *lat;
@property (nonatomic) NSString *lon;

@end

@implementation LocationPickerController{
    GMSPolygon *polygonLine;
    GMSMutablePath *polygonPoints;
    GMSMarker *markerForCenter;
    NSMutableArray *markers;
    NSNumber *polyArea;
    int editingMarker;
    BOOL *polygon;
    BOOL *byPoints;
    double xHigh;
    double xLow;
    double yHigh;
    double yLow;
    double polygonArea;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    NSLog(@"%f, %f", self.current.coordinate.latitude,self.current.coordinate.longitude);
    
    
    [self.mapView setDelegate:self];
    [self.mapView setMapType:kGMSTypeHybrid];
    [self.mapView setMyLocationEnabled:NO];
    [[self.mapView settings] setMyLocationButton:NO];
    
}

- (void)viewDidAppear:(BOOL)animated {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.current.coordinate.latitude
                                                           longitude:self.current.coordinate.longitude
                                                                zoom:20];
    [self.mapView setCamera:camera];
    
    if (self.reportEncodedPath) {
        NSLog(@"encoded path - %@",self.reportEncodedPath);
        GMSPolygon *aPolygon = [GMSPolygon polygonWithPath:[GMSPath pathFromEncodedPath:self.reportEncodedPath]];
        [aPolygon setMap:self.mapView];
        [self.polygonToolBar setHidden:NO];
        [self.pointPolyControl setSelectedSegmentIndex:1];
        [self.pointPolyControl setSelectedSegmentIndex:0];
        polygon = (BOOL*)YES;
        byPoints = (BOOL*)YES;
        GMSCoordinateBounds *newPosition = [[GMSCoordinateBounds alloc] initWithPath:[GMSPath pathFromEncodedPath:self.reportEncodedPath]];
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:newPosition];
        [self.mapView animateWithCameraUpdate:update];

        
    }else{
        GMSMarker *currentReportLoc = [GMSMarker markerWithPosition:self.current.coordinate];
        [currentReportLoc setMap:self.mapView];
        polygon = (BOOL*)NO;

        [self.polygonToolBar setHidden:YES];
    }
    NSLog(@"current coordinate - %f, %f", self.current.coordinate.latitude, self.current.coordinate.longitude);
    
    self.lat = [NSString stringWithFormat:@"%f",self.current.coordinate.latitude];
    self.lon = [NSString stringWithFormat:@"%f",self.current.coordinate.longitude];
    [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
    
    [super viewDidAppear:animated];

}

- (IBAction)pointPolygonSegmentControlDidChange:(id)sender {
    [self.mapView clear];
    if ([[sender titleForSegmentAtIndex:[sender selectedSegmentIndex]]isEqualToString:@"Polygon"]) {
        UISegmentedControl *drawBy = (UISegmentedControl*)[self.polygonToolBar viewWithTag:1];
        [drawBy setSelectedSegmentIndex:0];
        [self.polygonToolBar setHidden:NO];
        polygon = (BOOL*)YES;
        byPoints = (BOOL*)YES;
        
    }else{
        [self.polygonToolBar setHidden:YES];
        polygon = (BOOL*)NO;
        byPoints = (BOOL*)NO;
    }
}

- (IBAction)drawBySegmentControlDidChange:(id)sender {
    [self.mapView clear];
    if ([[sender titleForSegmentAtIndex:[sender selectedSegmentIndex]]isEqualToString:@"Points"]) {
        byPoints = (BOOL*)YES;
    }else{
        byPoints = (BOOL*)NO;
    }
}

- (IBAction)handleHelpTap:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Pin Help" message:@"Point Selected - You can drop a single point anywhere on the map. That point can be edited by press and holding on that point to initiate dragging.\n Polygon Selected - The first icon is for the \"Draw By Points\" option where you can simply drop points in a sequence to form a shape.  The second icon is for the \"Sketch\" option where you drop a single point first. Then you press and hold to initiate the drag feature and move the marker along the path of shape you wish to make." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)donePressed:(id)sender {
    if (!self.pickedLocation) {
        self.pickedLocation = [[NSMutableDictionary alloc]initWithObjects:@[[NSNumber numberWithDouble:[self.lat doubleValue]], [NSNumber numberWithDouble:[self.lon doubleValue]]] forKeys:@[@"lat",@"lng"]];
    }
    [self.delegate locationPickerController:self finishedPickingLocation:self.pickedLocation];
}

- (IBAction)cancelPressed:(id)sender {
    self.lat = nil;
    self.lon = nil;
    self.mapView = nil;
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)clearPressed:(id)sender {
    [self.mapView clear];
    [self.pickedLocation removeAllObjects];
    [markers removeAllObjects];
    [polygonPoints removeAllCoordinates];

}

- (IBAction)drawPressed:(id)sender {
    [self.mapView clear];
    
    GMSPolygon *sitePolygon = [GMSPolygon polygonWithPath:polygonPoints];
    [sitePolygon setFillColor:[UIColor colorWithRed:0 green:0 blue:.25 alpha:.25]];
    [sitePolygon setStrokeColor:[UIColor blueColor]];
    [sitePolygon setStrokeWidth:2];
    [sitePolygon setMap:self.mapView];
}

-(NSMutableDictionary*)computePolygonData:(GMSPath*)polygonPath{
    
    xLow = [polygonPath coordinateAtIndex:0].latitude;
    xHigh =[polygonPath coordinateAtIndex:0].latitude;
    yLow = [polygonPath coordinateAtIndex:0].longitude;
    yHigh =[polygonPath coordinateAtIndex:0].longitude;
    
    for (int i = 0; i<[polygonPath count]; i++) {
        CLLocationCoordinate2D toCompare = [polygonPath coordinateAtIndex:i];
        if (toCompare.latitude < xLow) {
            xLow = toCompare.latitude;
        }
        if (toCompare.latitude > xHigh) {
            xHigh = toCompare.latitude;
        }
        if (toCompare.longitude < yLow) {
            yLow = toCompare.longitude;
        }
        if (toCompare.longitude > yHigh) {
            yHigh = toCompare.longitude;
        }
    }
    double centerx = xLow+((xHigh - xLow)/2);
    double centery = yLow+((yHigh - yLow)/2);
    
    
    polygonArea = (GMSGeometryArea(polygonPath)*0.00024711); // Miles to Acres
    
    GMSMutablePath *closedPath = [[GMSMutablePath alloc]initWithPath:polygonPath];
    [closedPath addCoordinate:[polygonPath coordinateAtIndex:0]];
    
    return [NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:centerx],[NSNumber numberWithDouble:centery],[NSNumber numberWithDouble:polygonArea],[closedPath encodedPath]] forKeys:@[@"lat",@"lng",@"polygonArea",@"encodedPolygon"]];
}

#pragma mark - GMSMapViewDelegate
- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    
    if (!polygonPoints) {
        polygonPoints = [[GMSMutablePath alloc]init];
        markers = [[NSMutableArray alloc]init];
    }
    
    if (!polygon) {
        [self.mapView clear];
        
        self.lat = [NSString stringWithFormat:@"%f",coordinate.latitude];
        self.lon = [NSString stringWithFormat:@"%f",coordinate.longitude];
        [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
        
    }else if (polygon && !byPoints){
        [self.mapView clear];
        [markers removeAllObjects];
        [polygonPoints removeAllCoordinates];
        
        self.lat = [NSString stringWithFormat:@"%f",coordinate.latitude];
        self.lon = [NSString stringWithFormat:@"%f",coordinate.longitude];
        [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
        
    }else{
        [polygonPoints addCoordinate:coordinate];
        if (!polygonLine) {
            
            polygonLine = [[GMSPolygon alloc]init];
            [polygonLine setStrokeColor:[UIColor blueColor]];
            [polygonLine setStrokeWidth:4];
            
        }
        [polygonLine setMap:nil];
        [polygonLine setPath:polygonPoints];
        [polygonLine setMap:self.mapView];
        
        self.pickedLocation = [self computePolygonData:polygonPoints];
        
        [self.pinLatLong setText:[NSString stringWithFormat:@"Polygon Drawn:\n Approximatly %.4f Acres", [[self.pickedLocation valueForKey:@"polygonArea"] floatValue]]];
    }
    
    GMSMarker *startingPoint = [GMSMarker markerWithPosition:coordinate];
    [startingPoint setIcon:[UIImage imageNamed:@"CustomMarker.png"]];
    [startingPoint setGroundAnchor:CGPointMake(.5, .5)];
    [startingPoint setDraggable:YES];
    [startingPoint setMap:self.mapView];
    [markers addObject:startingPoint];
    
}

-(void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker{
    if (polygon) {
        if (!byPoints && editingMarker==([markers count]-1)) {
            if (!polygonLine) {
                polygonLine = [[GMSPolygon alloc]init];
                [polygonLine setStrokeColor:[UIColor blueColor]];
                [polygonLine setStrokeWidth:4];
            }
            
            [polygonLine setMap:nil];
            [polygonLine setPath:polygonPoints];
            [polygonLine setMap:self.mapView];
            [polygonPoints addCoordinate:[marker position]];
            if ([polygonPoints count]%5==0) {
                NSLog(@"adding new marker");
                GMSMarker *newMarker = [GMSMarker markerWithPosition:[marker position]];
                [newMarker setIcon:[UIImage imageNamed:@"CustomMarker.png"]];
                [newMarker setGroundAnchor:CGPointMake(.5, .5)];
                [newMarker setDraggable:YES];
                [newMarker setMap:self.mapView];
                [markers addObject:newMarker];
                
                editingMarker++;
            }
            
        }else{
            NSLog(@"editing index = %i", editingMarker);
            [polygonPoints replaceCoordinateAtIndex:editingMarker withCoordinate:[marker position]];
            [polygonLine setMap:nil];
            [polygonLine setPath:polygonPoints];
            [polygonLine setMap:self.mapView];
        }

    }
    self.lat = [NSString stringWithFormat:@"%f",marker.position.latitude];
    self.lon = [NSString stringWithFormat:@"%f",marker.position.longitude];
    [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
}

-(void)mapView:(GMSMapView *)aMapView didBeginDraggingMarker:(GMSMarker *)marker{
    if (polygon){
//        NSLog(@"markers count = %i",[markers count]);
        for (int i = 0;i<[markers count];i++){
            NSLog(@"index = %i",i);
            GMSMarker *aMarker = markers[i];
            NSLog(@" picked marker - %@, Array Marker %@", marker, aMarker);
            if ([marker isEqual:aMarker]) {
                editingMarker = i;
                NSLog(@"marker index = %i",editingMarker);
//                [polygonPoints removeCoordinateAtIndex:i];
                
                if (editingMarker==0) {
                    [markers removeAllObjects];
                    [polygonPoints removeAllCoordinates];
                    [polygonPoints addCoordinate:[marker position]];
                    GMSMarker *newStartingPoint = [GMSMarker markerWithPosition:[marker position]];
                    [newStartingPoint setIcon:[UIImage imageNamed:@"CustomMarker.png"]];
                    [newStartingPoint setGroundAnchor:CGPointMake(.5, .5)];
                    [newStartingPoint setDraggable:YES];
                    [newStartingPoint setMap:self.mapView];
                    [markers addObject:newStartingPoint];
                }
            }
        }
    }
    
    self.lat = [NSString stringWithFormat:@"%f",marker.position.latitude];
    self.lon = [NSString stringWithFormat:@"%f",marker.position.longitude];
    [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
}

-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    if (polygon) {
        [polygonPoints removeAllCoordinates];
        for (GMSMarker *aMarker in markers) {
            [polygonPoints addCoordinate:aMarker.position];
        }
        if (!polygonLine) {
            polygonLine = [[GMSPolygon alloc]init];
            [polygonLine setStrokeColor:[UIColor blueColor]];
            [polygonLine setStrokeWidth:4];
        }
        
        [polygonLine setMap:nil];
        [polygonLine setPath:polygonPoints];
        [polygonLine setMap:self.mapView];
        
        
        self.pickedLocation = [self computePolygonData:polygonPoints];
        GMSCoordinateBounds *newPosition = [[GMSCoordinateBounds alloc] initWithPath:polygonPoints];
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:newPosition];
        [self.mapView animateWithCameraUpdate:update];
        
        [self.pinLatLong setText:[NSString stringWithFormat:@"Polygon Drawn:\n Approximatly %.4f Acres", [[self.pickedLocation valueForKey:@"polygonArea"] floatValue]]];

    }else{
        self.lat = [NSString stringWithFormat:@"%f",marker.position.latitude];
        self.lon = [NSString stringWithFormat:@"%f",marker.position.longitude];
        [self.pinLatLong setText:[NSString stringWithFormat:@"Pin Location:\nLAT:%@ LNG:%@", self.lat, self.lon]];
    }
    
    
    
}




@end
