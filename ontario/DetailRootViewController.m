//
//  DetailRootViewController.m
//  ivegot1
//
//  Created by Jordan Daniel on 7/17/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "DetailRootViewController.h"
#import <objc/message.h>

@interface DetailRootViewController ()

@end

@implementation DetailRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    objc_msgSend([self.barButton target], [self.barButton action]);
//    [[self.barButton target] performSelector:[self.barButton action]];
}

-(void)viewDidDisappear:(BOOL)animated{
    if ([self.popover isPopoverVisible]) {
        [self.popover dismissPopoverAnimated:YES];
    }
}
#pragma mark - UISplitViewDelegate methods
-(void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc{
    NSLog(@"navtabs hidden");
    self.popover = pc;
    [barButtonItem setTitle:@"Navigation Tabs"];
    self.barButton = barButtonItem;
    [self.navBarItem setLeftBarButtonItem:barButtonItem];
}

-(void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"navtabs should appear");
    [self.navBarItem setLeftBarButtonItem:nil];
    self.popover = nil;
}


@end
