//
//  Report+CoreDataProperties.h
//  ontario
//
//  Created by Jordan Daniel on 10/5/15.
//  Copyright © 2015 com.bugwood. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Report.h"

NS_ASSUME_NONNULL_BEGIN

@interface Report (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *abundance;
@property (nullable, nonatomic, retain) NSNumber *accuracy;
@property (nullable, nonatomic, retain) NSNumber *amount;
@property (nullable, nonatomic, retain) NSNumber *area;
@property (nullable, nonatomic, retain) NSString *areaType;
@property (nullable, nonatomic, retain) NSString *commonName;
@property (nullable, nonatomic, retain) NSNumber *course;
@property (nullable, nonatomic, retain) NSString *coverage;
@property (nullable, nonatomic, retain) NSDate *dateTime;
@property (nullable, nonatomic, retain) NSString *dectMethod;
@property (nullable, nonatomic, retain) NSNumber *elavation;
@property (nullable, nonatomic, retain) NSString *habitat;
@property (nullable, nonatomic, retain) id images;
@property (nullable, nonatomic, retain) NSNumber *largest;
@property (nullable, nonatomic, retain) NSString *largestType;
@property (nullable, nonatomic, retain) NSNumber *lat;
@property (nullable, nonatomic, retain) NSNumber *lng;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *notes;
@property (nullable, nonatomic, retain) NSString *protocol;
@property (nullable, nonatomic, retain) NSNumber *reportType;
@property (nullable, nonatomic, retain) NSNumber *smallest;
@property (nullable, nonatomic, retain) NSString *smallestType;
@property (nullable, nonatomic, retain) id speciesDescriptions;
@property (nullable, nonatomic, retain) NSNumber *speed;
@property (nullable, nonatomic, retain) NSString *subID;
@property (nullable, nonatomic, retain) NSString *treeHost;

@end

NS_ASSUME_NONNULL_END
