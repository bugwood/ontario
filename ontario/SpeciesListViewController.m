//
//  SpeciesListViewController.m
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "SpeciesListViewController.h"
#import "AppDelegate.h"
#import "CustomTabController.h"
#import "PhotoViewController.h"
#import "SightingsController.h"
#import "ReportViewController.h"
#import "BWDataFileHandler.h"
#import "ImagePageViewController.h"
#import "WebViewController.h"
#import "UIImage+Resize.h"
//#import "ControlViewController.h"
//#import "TwoViewController.h"



@interface SpeciesListViewController ()
@property BOOL sortByCommon;
@property NSString *thumbnailCachePath;

@end

@implementation SpeciesListViewController

@synthesize sortByCommon;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[[UIDevice currentDevice]systemVersion] floatValue] < 7) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    sortByCommon = YES;
    if (self.selectedGroup == nil) {
        [self.navigationItem setTitle:@"All Species"];
        
        [self.searchBar setShowsScopeBar:NO];
        [self.searchBar sizeToFit];
        
        NSSortDescriptor *byCommon = [NSSortDescriptor sortDescriptorWithKey:@"commonNames" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        self.selectedGroup = [[[(AppDelegate*)[[UIApplication sharedApplication]delegate] fileHandler] GetAllSpecies] sortedArrayUsingDescriptors:@[byCommon]];
    }

    [self.searchBar setShowsScopeBar:NO];
    [self.searchBar sizeToFit];

    self.filteredArray = [NSMutableArray arrayWithCapacity:[self.selectedGroup count]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    BOOL isDir = NO;
    NSError *error;
    if (! [[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    self.thumbnailCachePath = [cachePath stringByAppendingString:@"Thumbnails"];
    
//    NSLog(@"selected group count = %i", [self.selectedGroup count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeSort:(id)sender {
    if (self.sortByCommon) {
        self.sortByCommon = NO;
        [self.changeSort setTitle:@"Common"];
    }else{
        [self.changeSort setTitle:@"Scientific"];
        self.sortByCommon = YES;
        
    }
    
    NSSortDescriptor *sortBy;
    if (!self.sortByCommon) {
        sortBy = [NSSortDescriptor sortDescriptorWithKey:@"sciname" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    }else{
        sortBy = [NSSortDescriptor sortDescriptorWithKey:@"commonNames" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    }
    self.selectedGroup = [self.selectedGroup sortedArrayUsingDescriptors:@[sortBy]];
    [self.tableView reloadData];
    
}

- (IBAction)editMySpeciesPressed:(id)sender {
    UIStoryboard *sb;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        sb = [UIStoryboard storyboardWithName:@"SB_iPad" bundle:[NSBundle mainBundle]];
    }else{
        sb = [UIStoryboard storyboardWithName:@"SB_iPhone" bundle:[NSBundle mainBundle]];
    }
    UINavigationController *pickerNav = [sb instantiateViewControllerWithIdentifier:@"SpeciesPicker"];
    SpeciesPickerViewController *speciesPicker = (SpeciesPickerViewController*)[pickerNav topViewController];
    [speciesPicker setDelegate:self];
    
    NSDictionary *species = [[(AppDelegate*)[[UIApplication sharedApplication]delegate]fileHandler] GetAllSpeciesForPicker];
    [speciesPicker setSpeciesList:species];
    [speciesPicker setSections:[[species allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)]];
    
    NSMutableDictionary *mySpecies = [[NSMutableDictionary alloc]initWithCapacity:[self.selectedGroup count]];
    for (int i = 0; i<[self.selectedGroup count]; i++) {
        NSDictionary *aSpecies = [self.selectedGroup objectAtIndex:i];
        [mySpecies setObject:aSpecies forKey:[[aSpecies valueForKey:@"subid"] stringValue]];
    }
    [speciesPicker setSpeciesData:mySpecies];
    
    
    [self presentViewController:pickerNav animated:YES completion:NULL];

    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UIStoryboard *storyBoard;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        storyBoard = [UIStoryboard storyboardWithName:@"SB_iPhone" bundle:[NSBundle mainBundle]];
    }else{
        UINavigationController *nav = [[[appDelegate splitViewController]viewControllers]objectAtIndex:1];
        [nav popToRootViewControllerAnimated:NO];
        storyBoard = [UIStoryboard storyboardWithName:@"SB_iPad" bundle:[NSBundle mainBundle]];
    }

    if ([segue.identifier isEqualToString:@"ViewSpeciDetailsSegue"]) {
        CustomTabController *tabController;
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            tabController = (CustomTabController*)segue.destinationViewController;
//        }else{
            tabController = (CustomTabController*)[(UINavigationController*)segue.destinationViewController topViewController];
//        }
        NSLog(@"species subid - %@",[sender valueForKey:@"subid"]);
        tabController.subid = [sender valueForKey:@"subid"];
        [tabController setTitle:[sender objectForKey:@"sciname"]];
        if ([[sender valueForKey:@"images"] count]>0) {
            ImagePageViewController *pageViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ImagesTab"];
            [pageViewController setDataSource:pageViewController];
            [pageViewController setIndex:0];
            [pageViewController setImageCount:[[sender valueForKey:@"images"]count]];
            [pageViewController setViewControllers:@[[PhotoViewController photoViewControllerForPageIndex:0 selectedSpecies:sender]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
            [tabController addChildViewController:pageViewController];
        }
        SightingsController *sightings = [storyBoard instantiateViewControllerWithIdentifier:@"SightingsTab"];
        sightings.speciSubid = [sender objectForKey:@"subid"];
        
        
        
        WebViewController *detailsViewController;
//        ControlViewController *controlView;
//        TwoViewController *twoViews;
//        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            detailsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DetailsTab"];
            [detailsViewController setDetailsHTML:[sender valueForKey:@"description"]];
//            controlView = [storyBoard instantiateViewControllerWithIdentifier:@"ControlTab"];
//            [controlView setControlText:[sender valueForKey:@"TreatmentOptions"]];
            [tabController addChildViewController:detailsViewController];
//            [tabController addChildViewController:controlView];
//        }else{
//            twoViews = [storyBoard instantiateViewControllerWithIdentifier:@"TwoViewTab"];
//            [twoViews setSelectedSpeci:sender];
//            [tabController addChildViewController:twoViews];
//        }
        
        
        [tabController addChildViewController:sightings];
        
    }else if ([segue.identifier isEqualToString:@"ReportSpeciesSegue"])
    {
        ReportViewController *reportController = segue.destinationViewController;
        Report *aReport = [[Report alloc] initWithEntity:[NSEntityDescription entityForName:@"Report" inManagedObjectContext:[appDelegate managedObjectContext]] insertIntoManagedObjectContext:nil];
        [aReport setDateTime:[NSDate date]];
        [aReport setName:[sender valueForKey:@"sciname"]];
        [aReport setCommonName:[sender valueForKey:@"commonName"]];
        [aReport setSubID:[[sender valueForKey:@"subid"]stringValue]];
        [aReport setAccuracy:[NSNumber numberWithDouble:appDelegate.currentloc.horizontalAccuracy]];
        [aReport setCourse:[NSNumber numberWithDouble:appDelegate.currentloc.horizontalAccuracy]];
        [aReport setElavation:[NSNumber numberWithDouble:appDelegate.currentloc.verticalAccuracy]];
        [aReport setLat:[NSNumber numberWithDouble:appDelegate.currentloc.coordinate.latitude]];
        [aReport setLng:[NSNumber numberWithDouble:appDelegate.currentloc.coordinate.longitude]];
        [aReport setSpeed:[NSNumber numberWithDouble:appDelegate.currentloc.speed]];
        [aReport setImages:[NSArray array]];
        
        if ([[sender valueForKey:@"taxonGroup"] isEqualToString:@"Aquatic Plants"])
        {
            [aReport setReportType:[NSNumber numberWithInteger:kReportTypeAquaticPlant]];
        }else if ([[sender valueForKey:@"taxonGroup"] isEqualToString:@"Aquatic Animals"])
        {
            [aReport setReportType:[NSNumber numberWithInteger:kReportTypeAquaticAnimal]];
        }else if ([[sender valueForKey:@"taxonGroup"] isEqualToString:@"Invasive Forest Pests"])
        {
            [aReport setReportType:[NSNumber numberWithInteger:kReportTypeForestPest]];
        }else if ([[sender valueForKey:@"taxonGroup"] isEqualToString:@"Terrestrial Plants"])
        {
            [aReport setReportType:[NSNumber numberWithInteger:kReportTypeTerrestialPlant]];
        }
        
        [reportController setAReport:aReport];
        
        [[NSNotificationCenter defaultCenter] addObserver:reportController
                                                 selector:@selector(updatedLocation:)
                                                     name:@"newLocationNotif"
                                                   object:nil];
    }
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.tableView.frame.origin) ) {
        [self.tableView scrollRectToVisible:self.tableView.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredArray count];
    }
    return [self.selectedGroup count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *aSpecies;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        aSpecies = [self.filteredArray objectAtIndex:indexPath.row];
    } else {
        aSpecies = [self.selectedGroup objectAtIndex:[indexPath row]];
    }
    
    static NSString *CellIdenitifer;
    if([[aSpecies valueForKey:@"sciname"] isEqualToString:@"N/A"]){
        CellIdenitifer = @"UnknownCell";
    }else{
        CellIdenitifer = @"SpeciCell";
    }
    
        
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdenitifer];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *detailTextLabel = (UILabel*)[cell viewWithTag:2];
    
    if (self.sortByCommon) {
        textLabel.text = [aSpecies valueForKey:@"commonNames"];
        [textLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
        detailTextLabel.text = [aSpecies valueForKey:@"sciname"];
        [detailTextLabel setFont:[UIFont italicSystemFontOfSize:16.0f]];
    }else{
        textLabel.text = [aSpecies valueForKey:@"sciname"];
        detailTextLabel.text = [aSpecies valueForKey:@"commonNames"];
        [textLabel setFont:[UIFont fontWithName:@"Helvetica-BoldOblique" size:17.0f]];
        [detailTextLabel setFont:[UIFont systemFontOfSize:16.0f]];
    }
    
    UIImageView *thumbnail = (UIImageView*)[cell viewWithTag:3];
    NSString *imageFilePath = [self.thumbnailCachePath stringByAppendingString:[[aSpecies valueForKey:@"subid"]stringValue]];
    thumbnail.image = [UIImage imageWithContentsOfFile:imageFilePath];
    
    if (!(thumbnail.image)){
        NSLog(@"having to resize image and store in cache %@", [aSpecies valueForKey:@"commonNames"]);
        __block NSString *keyToStore = [[aSpecies valueForKey:@"subid"]stringValue];
        if ([[aSpecies valueForKey:@"images"] count]>0) {
            dispatch_queue_t downloadThumbnailQueue = dispatch_queue_create("Get Photo Thumbnail", NULL);
            dispatch_async(downloadThumbnailQueue, ^{
                NSString *fullFilename = [[[aSpecies valueForKey:@"images"]objectAtIndex:0]objectForKey:@"filename"];
                NSString *filename = [fullFilename substringToIndex:[fullFilename length]-4];
                NSString *extension = [fullFilename substringFromIndex:[fullFilename length]-3];
                UIImage *imageToResize = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:filename ofType:extension]];
                CGSize newSize = CGSizeMake(200, 200);
                UIImage *image = [imageToResize resizedImageToFitInSize:newSize scaleIfSmaller:YES];
                [UIImageJPEGRepresentation(image, 100) writeToFile:[self.thumbnailCachePath stringByAppendingString:keyToStore] atomically:YES];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UITableViewCell *cellToUpdate = [self.tableView cellForRowAtIndexPath:indexPath]; // create a copy of the cell to avoid keeping a strong pointer to "cell" since that one may have been reused by the time the block is ready to update it.
                    if (cellToUpdate != nil) {
                        UIImageView *thumbnail = (UIImageView*)[cell viewWithTag:3];
                        [thumbnail setImage:image];
                        [cellToUpdate setNeedsLayout];
                    }
                });
            });

        }else{
            [thumbnail setImage:[UIImage imageNamed:@"noImage.png"]];
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"row selected not accessory view");
    NSDictionary *selectedSpeci;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        selectedSpeci = [self.filteredArray objectAtIndex:indexPath.row];
    } else {
        selectedSpeci = [self.selectedGroup objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"ReportSpeciesSegue" sender:selectedSpeci];
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *selectedSpeci;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        selectedSpeci = [self.filteredArray objectAtIndex:indexPath.row];
    } else {
        selectedSpeci = [self.selectedGroup objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"ViewSpeciDetailsSegue" sender:selectedSpeci];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSError *deleteMySpecies;
        NSNumber *subid = [[self.selectedGroup objectAtIndex:indexPath.row]objectForKey:@"subid"];
        [[[NSUserDefaults standardUserDefaults] objectForKey:@"MySpeciesList"] removeObject:subid];
        
		// Save the context.
		if (deleteMySpecies) {
			UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error removing selected Species from My List" message:deleteMySpecies.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
		}else{
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        
	}
}


#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredArray removeAllObjects];
    // Filter the array using NSPredicate
    
    NSPredicate *predicate;
    if ([scope isEqualToString:@"Scientific Name"]) {
        predicate = [NSPredicate predicateWithFormat:@"SELF.sciname contains[c] %@",searchText];
    }else{
        predicate = [NSPredicate predicateWithFormat:@"SELF.commonNames contains[c] %@",searchText];
    }
    self.filteredArray = [NSMutableArray arrayWithArray:[self.selectedGroup filteredArrayUsingPredicate:predicate]];
    
}


#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
    [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark - Species Picker Delegate method
-(void)speciesPickerViewController:(SpeciesPickerViewController *)speciesPicker finishedPickingSpecies:(NSDictionary *)pickedSpecies{
    
    NSLog(@"returning negative species list");
    self.selectedGroup = [[(AppDelegate*)[[UIApplication sharedApplication]delegate]fileHandler]GetSpecificSpecies:[pickedSpecies allKeys]];
    [[NSUserDefaults standardUserDefaults]setObject:[pickedSpecies allKeys] forKey:@"MySpeciesList"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.tableView reloadData];
    
    [speciesPicker dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
