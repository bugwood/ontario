//
//  CustomTabController.m
//  gledn
//
//  Created by Jordan Daniel on 4/9/14.
//  Copyright (c) 2014 com.bugwood. All rights reserved.
//

#import "CustomTabController.h"

@interface CustomTabController ()

@end

@implementation CustomTabController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"%@\n%@",(NSMutableArray*)[[NSUserDefaults standardUserDefaults]objectForKey:@"MySpeciesList"],self.subid);
    UIImage *favoriteBarImage;
    if ([(NSMutableArray*)[[NSUserDefaults standardUserDefaults]objectForKey:@"MySpeciesList"] containsObject:self.subid]) {
        favoriteBarImage = [UIImage imageNamed:@"726-star-selected@2x.png"];
    }else{
        favoriteBarImage = [UIImage imageNamed:@"726-star@2x.png"];
    }
    UIBarButtonItem *favoriteButton = [[UIBarButtonItem alloc]initWithImage:favoriteBarImage
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(addToMyListPressed:)];
    [self.navigationItem setRightBarButtonItem:favoriteButton];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismissButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)addToMyListPressed:(id)sender {
    NSMutableArray *mySpeciesList;
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"MySpeciesList"]) {
        mySpeciesList = [[NSMutableArray alloc]init];
    }else{
        mySpeciesList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]valueForKey:@"MySpeciesList"]];
    }
    if ([mySpeciesList containsObject:self.subid]) {
        [mySpeciesList removeObject:self.subid];
        [(UIBarButtonItem*)sender setImage:[UIImage imageNamed:@"726-star@2x.png"]];
    }else{
        [mySpeciesList addObject:self.subid];
        [(UIBarButtonItem*)sender setImage:[UIImage imageNamed:@"726-star-selected@2x.png"]];
    }
    [[NSUserDefaults standardUserDefaults]setObject:mySpeciesList forKey:@"MySpeciesList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

@end
