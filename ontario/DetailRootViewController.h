//
//  DetailRootViewController.h
//  ivegot1
//
//  Created by Jordan Daniel on 7/17/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailRootViewController : UIViewController <UISplitViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UINavigationItem *navBarItem;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) UIBarButtonItem *barButton;


@end
