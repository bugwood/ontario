//
//  AppDelegate.m
//  ontario
//
//  Created by Jordan Daniel on 10/15/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "AppDelegate.h"
#import "UIImage+Resize.h"
#import "DetailRootViewController.h"
#import "Flurry.h"
#import "GoogleMaps.h"
#import "AFNetworking.h"


@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"HQF472TK6F9ZBZ2NQY2M"];
    
    self.locManager = [[CLLocationManager alloc]init];
    [self.locManager setDelegate:self];
    [self.locManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locManager setDistanceFilter:10];
    
    [GMSServices provideAPIKey:@"AIzaSyBx6NGqvNxohwyt63rLCJiZfkks6_7TiyI"];
    
    self.fileHandler = [[BWDataFileHandler alloc]init];
    
    if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad) {
        self.splitViewController = (UISplitViewController*)self.window.rootViewController;
        [self.splitViewController setDelegate:(DetailRootViewController*)[[[self.splitViewController childViewControllers] objectAtIndex:1] topViewController]];
    }
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(remoteNotif)
    {
        //Handle remote notification
        NSLog(@"remote notifcation handled in application didFinishLaunching\n%@",remoteNotif);
        [self performSelector:@selector(receivedPushNotificationWhileInBackground:) withObject:[remoteNotif objectForKey:@"aps"] afterDelay:1];
    }

    return YES;
}

-(void)receivedPushNotificationWhileInBackground:(NSDictionary*)remoteNotif{
    NSLog(@"didFinishLaunching called receivedPushNotification");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HAS_PUSH_NOTIFICATION" object:remoteNotif];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"remote notification handled in didReceiveRemoteNotification\n%@",userInfo);
    if (application.applicationState==UIApplicationStateInactive) {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            [[(UINavigationController*)[[(UISplitViewController*)self.window.rootViewController viewControllers] objectAtIndex:0] topViewController]performSegueWithIdentifier:@"NewsFeedSegue" sender:[userInfo objectForKey:@"aps"]];
        }else{
            [[(UINavigationController*)self.window.rootViewController topViewController]performSegueWithIdentifier:@"NewsFeedSegue" sender:[userInfo objectForKey:@"aps"]];
        }
    }else{
        if ([UIAlertController class]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"New News Article Arrived!"
                                                                           message:[NSString stringWithFormat:@"\"%@\" has arrived in the News Feed.",[[userInfo objectForKey:@"aps"]objectForKey:@"alert"]]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *dismissAction = [UIAlertAction actionWithTitle:@"Dismiss"
                                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                        
                                                                    }];
            UIAlertAction *viewAction = [UIAlertAction actionWithTitle:@"View"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                     if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                                                                         [(UINavigationController*)[[(UISplitViewController*)self.window.rootViewController viewControllers] objectAtIndex:0] popToRootViewControllerAnimated:YES] ;
                                                                         [[(UINavigationController*)[[(UISplitViewController*)self.window.rootViewController viewControllers] objectAtIndex:0] topViewController]performSegueWithIdentifier:@"NewsFeedSegue" sender:[userInfo objectForKey:@"aps"]];
                                                                     }else{
                                                                         [(UINavigationController*)self.window.rootViewController popToRootViewControllerAnimated:YES];
                                                                         [[(UINavigationController*)self.window.rootViewController topViewController]performSegueWithIdentifier:@"NewsFeedSegue" sender:[userInfo objectForKey:@"aps"]];
                                                                     }
                                                                 }];
            [alert addAction:viewAction];
            [alert addAction:dismissAction];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Report Saved!" message:@"Your report has been added to the queue for upload." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *theToken = [NSString stringWithFormat:@"%@", deviceToken];
    NSString *editedToken = [theToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    editedToken = [editedToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    editedToken = [editedToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSLog(@"My token is: %@", editedToken);
    NSString *editedName = [[[UIDevice currentDevice] name] stringByReplacingOccurrencesOfString:@"'" withString:@""];
    
    NSLog(@"%@",editedName);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"39" forKey:@"project"];
    [params setObject:@"2" forKey:@"source"];
    [params setObject:editedToken forKey:@"token"];
    [params setObject:@"NJInvasives" forKey:@"userData"];
    
    AFHTTPRequestOperationManager *httpclient = [AFHTTPRequestOperationManager manager];
    [httpclient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [httpclient POST:@"https://www.eddmaps.org/phone/pushIPM/register.cfm"
          parameters:params
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                 NSLog(@"Request Successful, response '%@'", responseStr);
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error registering device - %@",error.description);
             }
     ];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.locManager stopUpdatingLocation];
    NSLog(@"Stopping updating loc");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"Starting updating loc");
    if ([self.locManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locManager requestWhenInUseAuthorization];
    }
    [self.locManager startUpdatingLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

-(NSInteger)numberOfReports{
    
    NSFetchRequest *posiReports = [[NSFetchRequest alloc]init];
    [posiReports setEntity:[NSEntityDescription entityForName:@"Report" inManagedObjectContext:self.managedObjectContext]];
    
    //    NSFetchRequest *negReports = [[NSFetchRequest alloc]init];
    //    [negReports setEntity:[NSEntityDescription entityForName:@"NegDataReport" inManagedObjectContext:self.managedObjectContext]];
    
    NSError *gettingReportsError;
    
    NSInteger reports = (NSInteger)([self.managedObjectContext countForFetchRequest:posiReports error:&gettingReportsError]);
    
    if (gettingReportsError) {
        NSLog(@"Error getting Reports - %@",gettingReportsError.localizedDescription);
    }else{
        return reports;
    }
    return 0;
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ontario" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ontario.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"loggedIn"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"ontario.sqlite"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            NSLog(@"file exsists and deleting it");
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]){
            NSLog(@"Unresolved error in persistant store coordinator: %@, %@", error, [error userInfo]);
            abort();
        }
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Location Manager Delegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    NSLog(@"recieved new loc");
    CLLocation *newLoc = [locations lastObject];
    if(!self.currentloc){
        NSLog(@"creating currentloc");
        self.currentloc = [[CLLocation alloc]init];
    }
    self.currentloc = newLoc;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newLocationNotif"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObject:newLoc forKey:@"newLocationResult"]];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
        NSLog(@"Location Services are off");
        if ([UIAlertController class]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"GPS Error"
                                                                           message:@"Location Services is turn off. Please go to \nSettings ->\nPrivacy ->\nLocation Services\n and ensure that the switch is green next to \"Location Services\" and \"NJ Invasives\" in the app list. Tap the Settings Button below to go straight to the Settings App."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                             }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:settings];
            [alert addAction:cancel];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
            
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"GPS Error"
                                                               message:@"Location Services is turn off. Please go to \nSettings ->\nPrivacy ->\nLocation Services\n and ensure that the switch is green next to \"Location Services\" and \"West\" in the app list"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
}


@end
