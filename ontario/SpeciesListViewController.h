//
//  SpeciesListViewController.h
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpeciesPickerViewController.h"

@interface SpeciesListViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate,SpeciesPickerDelegate>

@property(strong, nonatomic) NSArray *selectedGroup;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *changeSort;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong,nonatomic) NSMutableArray *filteredArray;
- (IBAction)changeSort:(id)sender;
- (IBAction)editMySpeciesPressed:(id)sender;

@end
