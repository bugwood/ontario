//
//  MoreTableController.m
//  srsfieldguide
//
//  Created by Jordan Daniel on 6/26/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "MoreTableController.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import <sys/utsname.h>

@interface MoreTableController ()
@end

@implementation MoreTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[[UIDevice currentDevice]systemVersion] floatValue] < 7) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    [self.versionLabel setText:[NSString stringWithFormat:@"Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 6;
//}


#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UIStoryboard *sb;
//    UINavigationController *nav;
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
//        sb = [UIStoryboard storyboardWithName:@"SB_iPhone" bundle:[NSBundle mainBundle]];
//        nav = self.navigationController;
//    }else{
//        sb = [UIStoryboard storyboardWithName:@"SB_iPad" bundle:[NSBundle mainBundle]];
//        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//        nav = [[[appDelegate splitViewController] viewControllers]objectAtIndex:1];
//        [nav popToRootViewControllerAnimated:NO];
//    }
//    switch ([indexPath row]) {
//        case 0:
//            {
//               UITableViewCell *cell= (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//                NSString *moreView = [[NSString alloc]initWithString:[[cell textLabel]text]];
//                UIViewController *nextView = (UIViewController *)[sb instantiateViewControllerWithIdentifier:moreView];
//                [nav pushViewController:nextView animated:YES];
//            }
//            break;
//        case 1:case 2:
//            {
//                UITableViewCell *cell= (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//                NSString *moreView = [[NSString alloc]initWithString:[[cell textLabel]text]];
//                WebViewController *webViewScene = (WebViewController*)[sb instantiateViewControllerWithIdentifier:@"webViewWithImage"];
//                [webViewScene setAboutPage:moreView];
//                [nav pushViewController:webViewScene animated:YES];
//
//            }
//            break;
//                case 3:
//            {
//                WebViewController *webViewScene = (WebViewController*)[sb instantiateViewControllerWithIdentifier:@"webViewController"];
//                [nav pushViewController:webViewScene animated:YES];
//            }
//            break;
//        case 4:
//            {
//                [self actionEmailComposer];
//            }
//    }
//}

- (IBAction)actionEmailComposer{
    
    if ([MFMailComposeViewController canSendMail]) {
        struct utsname systemInfo;
        uname(&systemInfo);
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setMailComposeDelegate:self];
        [mailViewController setToRecipients:@[@"eddmaps@ofah.org",@"cbargero@uga.edu",@"tjdaniel@uga.edu"]];
        [mailViewController setSubject:@"EDDMapS Ontario Feedback"];
        NSMutableString *body = [[NSMutableString alloc]init];
        [body appendString:[[NSString alloc]initWithFormat:@"\n\n\nDevice: %@\n", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]]];
        [body appendString:[[NSString alloc]initWithFormat:@"iOS Version: %@\n",[[UIDevice currentDevice]systemVersion]]];
        [body appendString:[[NSString alloc]initWithString:[NSString stringWithFormat:@"App Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]]];
        [mailViewController setMessageBody:body isHTML:NO];
        [self presentViewController:mailViewController animated:YES completion:NULL];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Unable to Send a Message" message:@"Sorry but the app is up able to send email. Be sure you have an activate email account on in your settings and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"Device is unable to send email in its current state.");
    }
    
}

#pragma mark - Mail Compose View delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UINavigationController *nav = [[[appDelegate splitViewController] viewControllers]objectAtIndex:1];
        [nav popToRootViewControllerAnimated:NO];
    }
    if ([segue.identifier isEqualToString:@"AboutEDDMapSSegue"]) {
        WebViewController *aboutEDDMapSView = segue.destinationViewController;
        [aboutEDDMapSView setDetailsHTML:@"<ul><li>Real time tracking of invasive species occurrences</li><li>Local and national distribution maps</li><li>Electronic early detection reporting tools</li><li>Library of identification and management information</li></ul><p><strong>Overview</strong></p><p>EDDMapS is a web-based mapping system for documenting invasive species distribution. It is fast, easy to use and doesn\'t require Geographic Information Systems experience. Launched in 2005 by the Center for Invasive Species and Ecosystem Health at the University of Georgia, it was originally designed as a tool for state Exotic Pest Plant Councils to develop more complete distribution data of invasive species.</p><p>EDDMapS Ontario was developed through the support and funding provided by the Canada/Ontario Invasive Species Centre, the Ontario Federation of Anglers and Hunters, and the Ontario Ministry of Natural Resources.</p><p>In Ontario, there are numerous agencies and monitoring programs in place that collect information and/or identify the distribution of invasive species. Monitoring programs generate large quantities of data, often covering a wide geographic area, multiple species, and lengthy time periods. Invasive species cross jurisdictional boundaries, so it is important to be able to share monitoring information with neighbouring provinces and states, and with the federal government. Effective data management improves our ability to detect and respond to invasive species, while avoiding duplication of effort.</p><p>Maximizing the effectiveness and accessibility of the immense numbers of invasive species observations recorded each year is a goal for EDDMapS. As of October 2013, EDDMapS contains more than 2.2 million records and 17,229 of these records are from Ontario.</p><p><strong>How does it work?</strong></p><p>EDDMapS documents the presence of invasive species. A simple, interactive Web interface enables participants to submit their observations or view results through interactive queries into the EDDMapS database. EDDMapS encourages users to participate by providing Internet tools that maintain their personal records and enable them to visualize data with interactive maps.</p><p>Users simply enter information from their observations into the standardized on-line data form, which allows specific information about the infestation and images to be added. Information entered is immediately loaded to the EDDMapS website for verification.</p><p>All information is reviewed by OFAH staff to ensure it is accurate before it is made viewable by the public. Once verified, the information is made freely available to scientists, researchers, land managers, land owners, educators, conservationists, ecologists, farmers, foresters, government personnel, and members of the public.</p><p>If you have any questions about EDDMapS Ontario, please contact the OFAH/OMNR Invading Species Awareness Program at 1-800-563-7711 or email <a href='mailto:eddmaps@ofah.org'>eddmaps@ofah.org</a>.</p>"];
    }
}


@end
