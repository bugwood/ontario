//
//  ImagePageViewController.h
//  seedn
//
//  Created by Jordan Daniel on 8/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePageViewController : UIPageViewController <UIPageViewControllerDataSource>

-(NSInteger)index;
-(NSInteger)imageCount;
-(void)setIndex:(NSUInteger)imageIndex;
-(void)setImageCount:(NSUInteger)speciImageCount;


@end
