//
//  BWDataFileHandler.h
//  njstriketeam
//
//  Created by Jordan Daniel on 2/27/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "EditSitesController.h"
//#import "Site.h"

@interface BWDataFileHandler : NSObject

//@property Site *currentSite;

-(NSArray*)GetGroupsFromDataFile;
-(NSArray*)GetAllSpecies;
-(NSDictionary*)GetAllSpeciesForPicker;
-(NSArray*)GetSpeciesForGroup:(NSString*)groupName;
-(NSArray*)GetSpecificSpecies:(NSArray*)speciesids;
//-(void)storeSites:(EditSitesController *)activeController;
//-(void)createRecentSiteTimer:(Site *)mostRecentSite;

@end
