//
//  ReportViewController.m
//  eddmapswest
//
//  Created by Jordan Daniel on 10/7/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "ReportViewController.h"
#import "AppDelegate.h"
#import "UIImage+Resize.h"
#import "AttributesPickerViewController.h"

@interface ReportViewController ()
@property NSManagedObjectContext *context;
@property UIImagePickerController *picker;
@property LocationPickerController *locPicker;
@property UITextField *currentTextField;
@property UIPickerView *pickerView;
@property UIToolbar *pickerBar;
@property NSArray *descriptions;
@property NSArray *habitats;
@property NSArray *abundances;
@property NSArray *coverages;
@property NSArray *detections;
@property BOOL aquatic;
@property BOOL terrestrial;
@property BOOL fishInvert;
@end

@implementation ReportViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.tabBarController) {
        [[self.tabBarController navigationItem] setRightBarButtonItem:self.addToQueue];
    }else{
        [self.navigationItem setRightBarButtonItem:self.addToQueue];
    }

    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.context = [appdelegate managedObjectContext];
    
    NSDictionary *pickerValues = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"PickerValues" ofType:@"plist"]];
    
    switch ([[self.aReport reportType]integerValue]) {
        case kReportTypeAquaticAnimal:
        {
            self.descriptions = [[pickerValues objectForKey:@"description"] objectForKey:@"Fish/Inverts"];
            
            if ([self.aReport largestType]==nil){
                [self.aReport setLargestType:@"mm"];
            }
            if ([self.aReport smallestType]==nil) {
                [self.aReport setSmallestType:@"mm"];
            }
        }
            break;
        case kReportTypeAquaticPlant:
        {
            self.descriptions = [[pickerValues objectForKey:@"description"] objectForKey:@"Aquatics"];
            
            if ([self.aReport areaType]==nil){
                NSLog(@"setting areatype");
                [self.aReport setAreaType:@"Hectares"];
            }
        }
            break;
        case kReportTypeForestPest:
        {
            self.descriptions = [[pickerValues objectForKey:@"description"] objectForKey:@"ForestPest"];
        }
            break;
        case kReportTypeTerrestialPlant:
        {
            self.descriptions = [[pickerValues objectForKey:@"description"] objectForKey:@"Terrestrials"];
            
            if ([self.aReport areaType]==nil){
                NSLog(@"setting areatype");
                [self.aReport setAreaType:@"Hectares"];
            }
        }
            break;
            
        default:
            break;
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)finishedEditing:(id)sender{
    [sender resignFirstResponder];
}

-(void)segmentControlPickedSegment:(id)sender{
    switch ([sender tag]) {
        case 1:
            [self.aReport setAreaType:[sender titleForSegmentAtIndex:[sender selectedSegmentIndex]]];
            break;
        case 2:
            [self.aReport setLargestType:[sender titleForSegmentAtIndex:[sender selectedSegmentIndex]]];
            break;
        case 3:
            [self.aReport setSmallestType:[sender titleForSegmentAtIndex:[sender selectedSegmentIndex]]];
            break;
        default:
            break;
    }
}

-(void)pickerDonePressed{
    [self finishedEditing:self.currentTextField];
}

- (IBAction)shootPic:(id)sender {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)pickPic:(id)sender {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}

-(IBAction)finishReport:(id)sender{
    BOOL missingInfo = NO;
    if (self.aquatic || self.terrestrial) {
        if (([self.aReport habitat]==nil || [[self.aReport habitat]isEqualToString:@""]) || ([self.aReport abundance]==nil || [[self.aReport abundance]isEqualToString:@""])) {
            missingInfo = YES;
        }
    }else{
        if (([self.aReport habitat]==nil || [[self.aReport habitat]isEqualToString:@""]) || ([self.aReport dectMethod]==nil || [[self.aReport habitat]isEqualToString:@""])) {
            missingInfo = YES;
        }
    }
    if(missingInfo){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Required Information Missing" message:@"If the species is an aquatic or terrestial plant the habitat and abundance fields are required. If the species is a fish or invertebrate then the habitat and method of detection fields are required. Please recheck your form and try to save again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }else if([self.aReport accuracy]==nil || [[self.aReport accuracy]floatValue]>100 ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"GPS Warning" message:@"Either the form hasn't found your GPS location or the accuracy isn't at an exceptable value. Would you like to wait for a more accurate value or would you like to choose a point?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Wait",@"Choose a Location", nil];
        [alert show];
        return;
    }else if([(NSArray*)[self.aReport images] count]==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Picture Warning" message:@"You haven't taken a picture, would you like to take one now?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
        [alert show];
        return;
    }else{
        [self saveReport];
    }
}

- (void)saveReport{
    NSError *err;
    if ([self.aReport managedObjectContext]==nil) {
        [self.context insertObject:self.aReport];

    }
    if (![self.context save:&err]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error saving report" message:[NSString stringWithFormat:@"Sorry, you are unable to save your report at this time. Error -%@", [err localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Report Saved!" message:@"Your report has been added to the queue for upload." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)pickLocation:(id)sender
{
    if (!(self.locPicker)) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SB_iPhone" bundle:[NSBundle mainBundle]];
        self.locPicker = [sb instantiateViewControllerWithIdentifier:@"locPicker"];
        [self.locPicker setDelegate:self];
        [self.locPicker setCurrent:[[CLLocation alloc] initWithLatitude:[[self.aReport lat] doubleValue] longitude:[[self.aReport lng] doubleValue]]];
    }
    
    [self presentViewController:self.locPicker animated:YES completion:NULL];
}

- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        if (!self.picker) {
            self.picker = [[UIImagePickerController alloc]init];
        }
        [self.picker setSourceType:sourceType];
        [self.picker setAllowsEditing:NO];
        [self.picker setDelegate:self];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            self.iPadPopoverView = [[UIPopoverController alloc]initWithContentViewController:self.picker];
            self.iPadPopoverView.delegate = self;
            [self.iPadPopoverView presentPopoverFromRect:CGRectMake(0, 0, 500, 700) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {
            [self presentViewController:self.picker animated:YES completion:NULL];
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Accessing Media" message:@"Device does not support chosen media." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

-(void) updatedLocation:(NSNotification*)notif {
    CLLocation* userLocation = (CLLocation*)[[notif userInfo] valueForKey:@"newLocationResult"];
    NSLog(@"recieved new location notificaton");
    [self.aReport setLat:[NSNumber numberWithDouble:userLocation.coordinate.latitude]];
    [self.aReport setLng:[NSNumber numberWithDouble:userLocation.coordinate.longitude]];
    [self.aReport setAccuracy:[NSNumber numberWithDouble:[userLocation horizontalAccuracy]]];
    [self.aReport setCourse:[NSNumber numberWithDouble:[userLocation course]]];
    [self.aReport setSpeed:[NSNumber numberWithDouble:[userLocation speed]]];
    [self.aReport setElavation:[NSNumber numberWithDouble:[userLocation altitude]]];
    [self.aReport setSpeed:[NSNumber numberWithDouble:[userLocation speed]]];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
        return 7;
    }
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
        case 1:
            return [(NSArray*)[self.aReport images] count]<5?[(NSArray*)[self.aReport images] count]+1:5;
        case 2:case 3:
            return 1;
        case 4:
            return [[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal?[[self.descriptions objectAtIndex:1]count]:[self.descriptions count];
        case 5:
            return [[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal?[[self.descriptions objectAtIndex:1]count]:1;
        case 6:
            return 1;
        default:
            break;
    }
    return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return nil;
        case 1:
            return @"Report Images";
        case 2:
            return @"GPS Location";
        case 3:
            return @"Observed Information";
        case 4:
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                return @"Description: Lifestage";
            }
            return @"Description - Select all that Apply";
            
        case 5:
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                return @"Description: Life Status";
            }
            return @"Notes";
        case 6:
            return @"Notes";
    }
    return nil;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch ([indexPath section]) {
        case 0:
            return 80;
        case 1:
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                return 350;
            }
            return 238;
        case 2:
            return 96;
        case 3:
            switch ([[self.aReport reportType]integerValue]) {
                case kReportTypeAquaticAnimal:
                    return 281;
                case kReportTypeAquaticPlant:
                    return 180;
                case kReportTypeForestPest:
                    return 214;
                case kReportTypeTerrestialPlant:
                    return 254;
            }
        case 4:
            return 30;
            
        case 5:
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                return 30;
            }
            return 100;
        case 6:
            return 100;
    }
    return 0;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    switch ([indexPath section]) {
        case 0:
            CellIdentifier = @"TimeStampCell";
            break;
        case 1:
            CellIdentifier = @"ReportImageCell";
            break;
        case 2:
            CellIdentifier = @"LocationCell";
            break;
        case 3:
            switch ([[self.aReport reportType]integerValue]) {
                case kReportTypeAquaticAnimal:
                    CellIdentifier = @"AnimalCell";
                    break;
                case kReportTypeAquaticPlant:
                    CellIdentifier = @"AquaticCell";
                    break;
                case kReportTypeForestPest:
                    CellIdentifier = @"ForestPestCell";
                    break;
                case kReportTypeTerrestialPlant:
                    CellIdentifier = @"TerrestrialCell";
                    break;
            }
            break;
        case 4:
            CellIdentifier = @"DescriptionCell";
            break;
        case 5:
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                CellIdentifier = @"DescriptionCell";
            }else{
                CellIdentifier = @"NotesCell";
            }
            break;
        case 6:
            CellIdentifier = @"NotesCell";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    switch ([indexPath section]) {
        case 0:
        {
            UILabel *sciLabel = (UILabel*)[cell viewWithTag:1];
            UILabel *comLabel = (UILabel*)[cell viewWithTag:2];
            UILabel *timeStamp = (UILabel*)[cell viewWithTag:3];
            
            [sciLabel setText:[self.aReport name]];
            [comLabel setText:[self.aReport commonName]];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [timeStamp setText:[formatter stringFromDate:[self.aReport dateTime]]];
        }
            break;
        case 1:
            if (indexPath.row<[(NSArray*)[self.aReport images]count]) {
                UIImageView *reportImage = (UIImageView*)[cell viewWithTag:5];
                [reportImage setImage:[UIImage imageWithData:[[(NSArray*)[self.aReport images]objectAtIndex:indexPath.row] objectForKey:@"reportImage"]]];
            }
            break;
        case 2:
        {
            
            UILabel *latitude = (UILabel*)[cell viewWithTag:1];
            UILabel *longitude = (UILabel*)[cell viewWithTag:2];
            UILabel *gpsAccuracy = (UILabel*)[cell viewWithTag:3];
        
            [latitude setText:[[self.aReport lat]stringValue]];
            [longitude setText:[[self.aReport lng]stringValue]];
            [gpsAccuracy setText:[[self.aReport accuracy]stringValue]];
            
        }
            break;
        case 3:
        {
            switch ([[self.aReport reportType]integerValue]) {
                case kReportTypeAquaticAnimal:
                {
                    UITextField *habitat = (UITextField*)[cell viewWithTag:1];
                    UITextField *detection = (UITextField*)[cell viewWithTag:5];
                    UITextField *amount = (UITextField*)[cell viewWithTag:6];
                    UITextField *sizeL = (UITextField*)[cell viewWithTag:7];
                    UITextField *sizeS = (UITextField*)[cell viewWithTag:8];
                    UITextField *protocol = (UITextField*)[cell viewWithTag:4];
                    
                    [habitat setDelegate:self];
                    [habitat setInputView:nil];
                    [habitat setInputAccessoryView:nil];
                    
                    [detection setDelegate:self];
                    [detection setInputView:nil];
                    [detection setInputAccessoryView:nil];
                    
                    [protocol setDelegate:self];
                    [protocol setInputView:nil];
                    [protocol setInputAccessoryView:nil];
                    
                    [amount setDelegate:self];
                    [sizeL setDelegate:self];
                    [sizeS setDelegate:self];
                    
                    [habitat setText:[self.aReport habitat]];
                    [detection setText:[self.aReport dectMethod]];
                    [amount setText:[[self.aReport amount]stringValue]];
                    [sizeL setText:[[self.aReport largest]stringValue]];
                    [sizeS setText:[[self.aReport smallest]stringValue]];
                    [protocol setText:[self.aReport protocol]];
                    
                    UISegmentedControl *sizeLType = (UISegmentedControl*)[cell viewWithTag:13];
                    [sizeLType addTarget:self
                                  action:@selector(segmentControlPickedSegment:)
                        forControlEvents:UIControlEventValueChanged];
                    
                    if ([[self.aReport largestType]isEqualToString:@"in"]) {
                        [sizeLType setSelectedSegmentIndex:2];
                    }else if ([[self.aReport largestType]isEqualToString:@"cm"]) {
                        [sizeLType setSelectedSegmentIndex:1];
                    }else{
                        [sizeLType setSelectedSegmentIndex:0];
                    }
                    
                    UISegmentedControl *sizeSType = (UISegmentedControl*)[cell viewWithTag:14];
                    [sizeSType addTarget:self
                                  action:@selector(segmentControlPickedSegment:)
                        forControlEvents:UIControlEventValueChanged];
                    
                    if ([[self.aReport smallestType]isEqualToString:@"in"]) {
                        [sizeSType setSelectedSegmentIndex:2];
                    }else if ([[self.aReport smallestType]isEqualToString:@"cm"]) {
                        [sizeSType setSelectedSegmentIndex:1];
                    }else{
                        [sizeSType setSelectedSegmentIndex:0];
                    }
                }
                    break;
                case kReportTypeAquaticPlant:
                {
                    UITextField *habitat = (UITextField*)[cell viewWithTag:1];
                    UITextField *abundance = (UITextField*)[cell viewWithTag:2];
                    UITextField *area = (UITextField*)[cell viewWithTag:3];
                    UITextField *protocol = (UITextField*)[cell viewWithTag:4];
                    
                    [habitat setDelegate:self];
                    [habitat setInputView:nil];
                    [habitat setInputAccessoryView:nil];
                    
                    [abundance setDelegate:self];
                    [abundance setInputView:nil];
                    [abundance setInputAccessoryView:nil];
                    
                    [area setDelegate:self];
                    
                    [protocol setDelegate:self];
                    [protocol setInputView:nil];
                    [protocol setInputAccessoryView:nil];
                    
                    UISegmentedControl *areaType = (UISegmentedControl*)[cell viewWithTag:12];
                    [areaType addTarget:self
                                 action:@selector(segmentControlPickedSegment:)
                       forControlEvents:UIControlEventValueChanged];
                    
                    [habitat setText:[self.aReport habitat]];
                    [abundance setText:[self.aReport abundance]];
                    [area setText:[[self.aReport area]stringValue]];
                    [protocol setText:[self.aReport protocol]];
                    
                    if ([[self.aReport areaType]isEqualToString:@"Sq Meters"]) {
                        [areaType setSelectedSegmentIndex:1];
                    }else{
                        [areaType setSelectedSegmentIndex:0];
                    }
                }
                    break;
                case kReportTypeForestPest:
                {
                    UITextField *habitat = (UITextField*)[cell viewWithTag:1];
                    UITextField *treeHost = (UITextField*)[cell viewWithTag:9];
                    UITextField *coverage = (UITextField*)[cell viewWithTag:10];
                    UITextField *area = (UITextField*)[cell viewWithTag:3];
                    UITextField *protocol = (UITextField*)[cell viewWithTag:4];
                    
                    [habitat setDelegate:self];
                    [habitat setInputView:nil];
                    [habitat setInputAccessoryView:nil];
                    
                    [treeHost setDelegate:self];
                    [treeHost setInputView:nil];
                    [treeHost setInputAccessoryView:nil];
                    
                    [coverage setDelegate:self];
                    [coverage setInputView:nil];
                    [coverage setInputAccessoryView:nil];
                    
                    [protocol setDelegate:self];
                    [protocol setInputView:nil];
                    [protocol setInputAccessoryView:nil];
                    
                    [area setDelegate:self];
                    
                    [habitat setText:[self.aReport habitat]];
                    [treeHost setText:[self.aReport abundance]];
                    [area setText:[[self.aReport area]stringValue]];
                    [coverage setText:[self.aReport coverage]];
                    [protocol setText:[self.aReport protocol]];
                    
                    UISegmentedControl *areaType = (UISegmentedControl*)[cell viewWithTag:12];
                    [areaType addTarget:self
                                 action:@selector(segmentControlPickedSegment:)
                       forControlEvents:UIControlEventValueChanged];
                    
                    if ([[self.aReport areaType]isEqualToString:@"Hectares"]) {
                        [areaType setSelectedSegmentIndex:0];
                    }else{
                        [areaType setSelectedSegmentIndex:1];
                    }
                }
                    break;
                case kReportTypeTerrestialPlant:
                {
                    UITextField *habitat = (UITextField*)[cell viewWithTag:1];
                    UITextField *abundance = (UITextField*)[cell viewWithTag:2];
                    UITextField *coverage = (UITextField*)[cell viewWithTag:10];
                    UITextField *area = (UITextField*)[cell viewWithTag:3];
                    UITextField *amount = (UITextField*)[cell viewWithTag:6];
                    UITextField *protocol = (UITextField*)[cell viewWithTag:4];
                    
                    [habitat setDelegate:self];
                    [habitat setInputView:nil];
                    [habitat setInputAccessoryView:nil];
                    
                    [abundance setDelegate:self];
                    [abundance setInputView:nil];
                    [abundance setInputAccessoryView:nil];
                    
                    [coverage setDelegate:self];
                    [coverage setInputView:nil];
                    [coverage setInputAccessoryView:nil];
                    
                    [protocol setDelegate:self];
                    [protocol setInputView:nil];
                    [protocol setInputAccessoryView:nil];
                    
                    [area setDelegate:self];
                    [amount setDelegate:self];
                    
                    UISegmentedControl *areaType = (UISegmentedControl*)[cell viewWithTag:12];
                    [areaType addTarget:self
                                 action:@selector(segmentControlPickedSegment:)
                       forControlEvents:UIControlEventValueChanged];
                    
                    [habitat setText:[self.aReport habitat]];
                    [abundance setText:[self.aReport abundance]];
                    [area setText:[[self.aReport area]stringValue]];
                    [amount setText:[[self.aReport amount]stringValue]];
                    [coverage setText:[self.aReport coverage]];
                    [protocol setText:[self.aReport protocol]];
                    
                    if ([[self.aReport areaType]isEqualToString:@"Hectares"]) {
                        [areaType setSelectedSegmentIndex:0];
                    }else{
                        [areaType setSelectedSegmentIndex:1];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 4:
        {
            NSString *descript;
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                descript =[[self.descriptions objectAtIndex:1] objectAtIndex:[indexPath row]];
            }else{
                descript =[self.descriptions objectAtIndex:[indexPath row]];
            }
            [[cell textLabel]setText:descript];
            if ([[self.aReport speciesDescriptions] containsObject:descript]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }else{
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
            break;
        case 5:
        {
            if ([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal) {
                NSString *descript = [[self.descriptions objectAtIndex:0] objectAtIndex:[indexPath row]];
                [[cell textLabel]setText:descript];
                if ([[self.aReport speciesDescriptions] containsObject:descript]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }else{
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
            }else{
                UITextView *reportNotes = (UITextView*)[cell viewWithTag:4];
                [reportNotes setDelegate:self];
                if ([self.aReport notes]) {
                    [reportNotes setTextColor:[UIColor blackColor]];
                    [reportNotes setText:[self.aReport notes]];
                }
            }
        }
            break;
        case 6:
        {
            UITextView *reportNotes = (UITextView*)[cell viewWithTag:4];
            [reportNotes setDelegate:self];
            if ([self.aReport notes]) {
                [reportNotes setTextColor:[UIColor blackColor]];
                [reportNotes setText:[self.aReport notes]];
            }
        }
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section]==4 || (([[self.aReport reportType]integerValue]==kReportTypeAquaticAnimal)&&[indexPath section]==5)) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if ([cell accessoryType]==UITableViewCellAccessoryNone) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [self.aReport addSpeciDescription:[[cell textLabel]text]];
        }else{
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [self.aReport removeSpeciDescription:[[cell textLabel]text]];
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - Text view delegate
- (void) textViewDidBeginEditing:(UITextView *) textView {
    if ([[textView text]isEqualToString:@"Additional information goes here. Do not include personal information. All information entered in this field will be displayed publicly."]) {
        [textView setText:@""];
        [textView setTextColor:[UIColor blackColor]];
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [self.aReport setNotes:[textView text]];
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

#pragma mark - TextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.currentTextField = textField;
    
    NSDictionary *pickerValues = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"PickerValues" ofType:@"plist"]];
    NSArray *attributes;
    
    UIStoryboard *sb;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        sb = [UIStoryboard storyboardWithName:@"SB_iPad" bundle:[NSBundle mainBundle]];
    }else{
        sb = [UIStoryboard storyboardWithName:@"SB_iPhone" bundle:[NSBundle mainBundle]];
    }
    
    UINavigationController *pickerNav = [sb instantiateViewControllerWithIdentifier:@"AttributesPicker"];
    AttributesPickerViewController *attributesPicker = (AttributesPickerViewController*)[pickerNav topViewController];
    [attributesPicker setDelegate:self];
    
//    [self.pickerView reloadAllComponents];
    switch (textField.tag) {
        case 1:
        {
            if ([[self.aReport reportType] integerValue]<3) {
                attributes = [[pickerValues objectForKey:@"habitats"]objectForKey:@"Aquatics"];
            }else{
                attributes = [[pickerValues objectForKey:@"habitats"]objectForKey:@"Terrestrial"];
            }
            [attributesPicker setMultipuleSelection:NO];
        }
            break;
        case 2:
        {
            attributes = [pickerValues objectForKey:@"abundance"];
            [attributesPicker setMultipuleSelection:NO];
        }
            break;
        case 3:
        {
            attributesPicker = nil;
            attributes = nil;
            pickerValues = nil;
            return;
        }
            break;
        case 4:
        {
            attributes = [pickerValues objectForKey:@"protocols"];
            [attributesPicker setMultipuleSelection:NO];
        }
            break;
        case 5:
        {
            attributes = [pickerValues objectForKey:@"detections"];
            [attributesPicker setMultipuleSelection:NO];
        }
            break;
        case 6:case 7:case 8:case 9:
        {
            attributesPicker = nil;
            attributes = nil;
            pickerValues = nil;
            return;
        }
        case 10:
        {
            attributes = [pickerValues objectForKey:@"coverage"];
            [attributesPicker setMultipuleSelection:NO];
        }
            break;
            
        default:
            break;
    }
    if (attributes) {
        [attributesPicker setAttributues:attributes];
        [self presentViewController:pickerNav animated:YES completion:nil];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    switch ([textField tag]) {
        case 3:
            [self.aReport setArea:[NSNumber numberWithInteger:[[textField text]integerValue]]];
            self.currentTextField=nil;
            break;
        case 6:
            [self.aReport setAmount:[NSNumber numberWithInteger:[[textField text]integerValue]]];
            self.currentTextField=nil;
            break;
        case 7:
            [self.aReport setLargest:[NSNumber numberWithDouble:[[textField text]doubleValue]]];
            self.currentTextField=nil;
            break;
        case 8:
            [self.aReport setSmallest:[NSNumber numberWithDouble:[[textField text]doubleValue]]];
            self.currentTextField=nil;
            break;
        case 9:
            [self.aReport setTreeHost:[textField text]];
            self.currentTextField=nil;
            break;
        default:
            break;
    }
    
}

#pragma mark - image picker controller delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UILabel *processingLabel = [[UILabel alloc]init];
    [processingLabel setTextAlignment:NSTextAlignmentCenter];
    [processingLabel setText:@"Processing \nPhoto.."];
    
    UIImageView *imageView = (UIImageView*)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]] viewWithTag:5];
    [imageView setImage:nil];
    [imageView addSubview:processingLabel];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [self.iPadPopoverView dismissPopoverAnimated:YES];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [self performSelectorInBackground:@selector(proccessPhoto:) withObject:info];
}

-(void)proccessPhoto:(NSDictionary*)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    CGSize newSize = CGSizeMake(chosenImage.size.width/3, chosenImage.size.height/3);
    UIImage *imageToDisplay = [chosenImage resizedImageToSize:newSize];
    
//    [self.aReport setImageData:UIImageJPEGRepresentation(imageToDisplay, 1.0)];
    
    [self performSelectorOnMainThread:@selector(finishedProcessing:) withObject:imageToDisplay waitUntilDone:YES];
}

-(void)finishedProcessing:(UIImage*)dislayImage{
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Location picker delegate
-(void)usePickedLocation:(NSDictionary *)pickedLocation{
    [self.locPicker dismissViewControllerAnimated:YES completion:NULL];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    NSLog(@"%f , %f",[[pickedLocation valueForKey:@"lat"]doubleValue],[[pickedLocation valueForKey:@"lng"]doubleValue]);
    
    [self.aReport setLat:[NSNumber numberWithDouble:[[pickedLocation valueForKey:@"lat"]doubleValue]]];
    [self.aReport setLng:[NSNumber numberWithDouble:[[pickedLocation valueForKey:@"lng"]doubleValue]]];
    [self.aReport setAccuracy:[NSNumber numberWithInt:0]];
    [self.aReport setCourse:[NSNumber numberWithInt:0]];
    [self.aReport setSpeed:[NSNumber numberWithInt:0]];
    [self.aReport setElavation:[NSNumber numberWithInt:0]];
    [self.aReport setSpeed:[NSNumber numberWithInt:0]];
    
    self.locPicker = nil;
    
    NSLog(@"%@ , %@",[self.aReport lat],[self.aReport lng]);
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Alertview delgate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([[alertView title] isEqualToString:@"Picture Warning"]) {
        switch (buttonIndex) {
            case 0:
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                break;
            case 1:
                [self saveReport];
                break;
            default:
                break;
        }
    }else if ([[alertView title] isEqualToString:@"GPS Warning"]) {
        switch (buttonIndex) {
            case 0:
                [alertView dismissWithClickedButtonIndex:0 animated:YES];
                break;
            case 1:
                [self pickLocation:nil];
                break;
            default:
                break;
        }
    }else if([[alertView title]isEqualToString:@"Required Information Missing"]){
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }else if([[alertView title]isEqualToString:@"Report Saved!"]) {
        switch (buttonIndex) {
            case 0:
                if (self.queueList !=nil) {
                    [[self.queueList tableView]reloadData];
                }else{
                }
                self.aReport = nil;
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }
}

-(void)AttributesViewController:(AttributesPickerViewController *)attributesPicker finishedPickingAttributes:(NSArray *)pickedAttributes{
    NSLog(@"current textfield tag - %li,\n%@",(long)[self.currentTextField tag],pickedAttributes);
    [attributesPicker dismissViewControllerAnimated:YES completion:nil];
    NSString *newAttribute = [pickedAttributes objectAtIndex:0];
    [self.currentTextField setText:newAttribute];
    switch (self.currentTextField.tag) {
        case 1:
        {
            [self.aReport setHabitat:newAttribute];
        }
            break;
        case 2:
        {
            [self.aReport setAbundance:newAttribute];
        }
            break;
        case 4:
        {
            [self. aReport setProtocol:newAttribute];
        }
            break;
        case 5:
        {
            [self.aReport setDectMethod:newAttribute];
        }
            break;
        case 10:
        {
            [self.aReport setCoverage:newAttribute];
        }
        default:
            break;
    }
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:3]] withRowAnimation:UITableViewRowAnimationAutomatic];
    self.currentTextField = nil;
}

-(void)canceledPickingAttributes:(AttributesPickerViewController *)attributesPicker{
    [attributesPicker dismissViewControllerAnimated:YES completion:nil];
    self.currentTextField = nil;
}


@end
