//
//  ArticleViewController.h
//  mrwc
//
//  Created by Jordan Daniel on 12/9/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleViewController : UIViewController
@property (strong, nonatomic) NSDictionary *anArticle;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishDate;
@property (weak, nonatomic) IBOutlet UIWebView *articleDecript;
@property (weak, nonatomic) IBOutlet UILabel *articleSource;


@end
