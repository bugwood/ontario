//
//  QueueViewController.h
//  srsfieldguide
//
//  Created by Jordan Daniel on 6/20/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QueueViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@property(strong, nonatomic)NSFetchedResultsController *fetchedResultsController;

-(IBAction)uploadButton;
@end
