//
//  SightingsController.m
//  gledn
//
//  Created by Jordan Daniel on 7/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "SightingsController.h"

@interface SightingsController ()

@property (strong, nonatomic)UIActivityIndicatorView *activityIndicator;

@end

@implementation SightingsController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityIndicator setFrame:CGRectMake(0, 0, 150, 150)];
    [self.activityIndicator setBackgroundColor:[UIColor grayColor]];
    self.activityIndicator.layer.cornerRadius = 10.0f;
    [self.activityIndicator setAlpha:.6];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator setCenter:self.view.center];
    [self.view addSubview:self.activityIndicator];
    
    
    [self.sightingsMap setDelegate:self];
    NSLog(@"%@",[NSString stringWithFormat:@"https://mobile.eddmaps.org/google/viewmap.cfm?sub=%li",(long)[self.speciSubid integerValue] ]);
    
    NSURL *getSightingsURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://mobile.eddmaps.org/google/viewmap.cfm?sub=%li",(long)[self.speciSubid integerValue]]];
    
    NSURLRequest *getSightings = [[NSURLRequest alloc]initWithURL:getSightingsURL];
	
    [self.sightingsMap loadRequest:getSightings];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)browserBackPressed:(id)sender {
    [self.sightingsMap goBack];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.activityIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    if ([webView canGoBack])
    {
        NSLog(@"Back button should be enabled");
        [self.browserBackItem setEnabled:YES];
    }
    else
    {
        NSLog(@"Back button should be disabled");
        [self.browserBackItem setEnabled:NO];
    }
    
    [self.activityIndicator stopAnimating];
    
}


@end
