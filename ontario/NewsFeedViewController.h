//
//  NewsFeedViewController.h
//  mrwc
//
//  Created by Jordan Daniel on 12/8/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedViewController : UITableViewController
@property NSDictionary *remoteNotif;
@end
