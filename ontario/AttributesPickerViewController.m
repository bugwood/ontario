//
//  AttributesPickerViewController.m
//  hthc
//
//  Created by Jordan Daniel on 10/27/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import "AttributesPickerViewController.h"
//#import "Site.h"
#import "AppDelegate.h"

@interface AttributesPickerViewController ()

@end

@implementation AttributesPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (!self.selectedAttributes) {
        self.selectedAttributes = [[NSMutableArray alloc]init];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
    if (!self.selectedAttributes || [self.selectedAttributes count]<1) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Attributes selected"
                                                       message:@"There seems to be no values selected. If you wish to dismiss without selecting anything please use Cancel"
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
    }else{
        [self.delegate AttributesViewController:self finishedPickingAttributes:self.selectedAttributes];
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.delegate canceledPickingAttributes:self];
}
- (IBAction)newSiteTapped:(id)sender {
//    UITableViewCell *newSiteCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    UITextField *newSiteName = (UITextField*) [newSiteCell viewWithTag:100];
//    if([newSiteName.text isEqualToString:@""]){
//        if ([UIAlertController class]) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Site Name"
//                                                                           message:@"Please provide a site name in the space provided"
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
//                                                              style:UIAlertActionStyleDefault
//                                                            handler:^(UIAlertAction *action) {
//                                                                [alert dismissViewControllerAnimated:YES completion:nil];
//                                                            }];
//            [alert addAction:dismiss];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }else{
//        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
////        Site *newSite = [NSEntityDescription insertNewObjectForEntityForName:@"Site" inManagedObjectContext:appDelegate.managedObjectContext];
////        [newSite setName:newSiteName.text];
//        [appDelegate.managedObjectContext save:nil];
//        [self.delegate AttributesViewController:self finishedPickingAttributes:@[newSite]];
//    }
//    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
//    if ([[self.attributues objectAtIndex:0]isKindOfClass:[Site class]]) {
//        return [self.attributues count]+1;
//    }
    return [self.attributues count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
//    if ([[self.attributues objectAtIndex:0]isKindOfClass:[Site class]]) {
//        if (indexPath.row == 0) {
//            cell = [tableView dequeueReusableCellWithIdentifier:@"NewAttributeCell" forIndexPath:indexPath];
//        }else{
//            cell = [tableView dequeueReusableCellWithIdentifier:@"AttributeCell" forIndexPath:indexPath];
//            Site *aSite = [self.attributues objectAtIndex:indexPath.row-1];
//            [cell.textLabel setText:[aSite name]];
//        }
//    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"AttributeCell" forIndexPath:indexPath];
        [cell.textLabel setText:[self.attributues objectAtIndex:indexPath.row]];
//    }
    

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id selectedAttribute;
//    if ([[self.attributues objectAtIndex:0]isKindOfClass:[Site class]]) {
//        selectedAttribute = [self.attributues objectAtIndex:indexPath.row-1];
//    }else{
        selectedAttribute = [self.attributues objectAtIndex:indexPath.row];
//    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (![self.selectedAttributes containsObject:selectedAttribute]) {
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        [self.selectedAttributes addObject:selectedAttribute];
        if (!self.multipuleSelection) {
            [self.delegate AttributesViewController:self finishedPickingAttributes:self.selectedAttributes];
        }
    }else{
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        [self.selectedAttributes removeObject:selectedAttribute];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
