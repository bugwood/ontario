//
//  ImagePageViewController.m
//  seedn
//
//  Created by Jordan Daniel on 8/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "ImagePageViewController.h"
#import "PhotoViewController.h"

@interface ImagePageViewController ()
{
    NSUInteger _index;
    NSUInteger _imagesCount;
}

@end

@implementation ImagePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSInteger)index
{
    return _index;
}

-(void)setIndex:(NSUInteger)imageIndex
{
    _index = imageIndex;
}

-(NSInteger)imageCount
{
    return _imagesCount;
}

-(void)setImageCount:(NSUInteger)speciImageCount
{
    _imagesCount = speciImageCount;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *subviews = self.view.subviews;
    UIPageControl *pageControl = nil;
    for (int i=0; i<[subviews count]; i++) {
        if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
            pageControl = (UIPageControl *)[subviews objectAtIndex:i];
        }
    }
    [pageControl setCurrentPageIndicatorTintColor:[UIColor blackColor]];
    [pageControl setPageIndicatorTintColor:[UIColor grayColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPageController Data Source Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerBeforeViewController:(PhotoViewController *)vc
{
    NSUInteger index = vc.pageIndex;
    if(_index>0){
        _index--;
    }
    return [PhotoViewController photoViewControllerForPageIndex:(index - 1)selectedSpecies:vc.selectedSpeci];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerAfterViewController:(PhotoViewController *)vc
{
    NSUInteger index = vc.pageIndex;
    if(_index < _imagesCount-1){
        _index++;
    }
    return [PhotoViewController photoViewControllerForPageIndex:(index + 1)selectedSpecies:vc.selectedSpeci];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return _imagesCount;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return _index;
}



@end
