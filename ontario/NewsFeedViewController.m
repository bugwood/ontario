//
//  NewsFeedViewController.m
//  mrwc
//
//  Created by Jordan Daniel on 12/8/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "ArticleViewController.h"
#import "AFNetworking.h"

@interface NewsFeedViewController ()
@property UIActivityIndicatorView *activityIndicator;
@property NSArray *newsArticles;
//@property UIRefreshControl *refreshControl;
@end

@implementation NewsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityIndicator setFrame:CGRectMake(0, 0, 150, 150)];
    [self.activityIndicator setBackgroundColor:[UIColor grayColor]];
    self.activityIndicator.layer.cornerRadius = 10.0f;
    [self.activityIndicator setAlpha:.6];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator setCenter:self.view.center];
    [self.view addSubview:self.activityIndicator];

    [self.activityIndicator startAnimating];
    [self refreshNewsFeed];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self
                            action:@selector(refreshNewsFeed)
                  forControlEvents:UIControlEventValueChanged];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshNewsFeed{
    AFHTTPRequestOperationManager *httpClient = [AFHTTPRequestOperationManager manager];
    httpClient.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [httpClient POST:@"https://eddmaps.org/phone/pushIPM/newsfeed.cfm?project=39" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        formData = nil;
    }success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Request Successful, response '%@'", responseStr);
        self.newsArticles = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        
        if ([self.activityIndicator isAnimating]) {
            [self.activityIndicator stopAnimating];
        }else if ([self.refreshControl isRefreshing]){
            [self.refreshControl endRefreshing];
        }
        
        if (self.remoteNotif) {
            for (NSDictionary* anArticle in self.newsArticles) {
                NSLog(@"%i %i",(int)[anArticle objectForKey:@"id"],(int)[self.remoteNotif objectForKey:@"newsId"]);
                if ((int)[anArticle objectForKey:@"id"]==(int)[self.remoteNotif objectForKey:@"newsId"]) {
                    NSLog(@"should preform push for notif");
                    [self performSegueWithIdentifier:@"ViewArticleSegue" sender:anArticle];
                }
            }
        }else{
            [self.tableView reloadData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        if ([self.activityIndicator isAnimating]) {
            [self.activityIndicator stopAnimating];
        }else if ([self.refreshControl isRefreshing]){
            [self.refreshControl endRefreshing];
        }
        
    }];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.newsArticles count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell" forIndexPath:indexPath];
    NSDictionary *anArticle = [self.newsArticles objectAtIndex:indexPath.row];
    [cell.textLabel setText:[anArticle objectForKey:@"title"]];
    
    NSDateFormatter *informat = [[NSDateFormatter alloc]init];
    [informat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *publishedDate = [informat dateFromString:[anArticle objectForKey:@"published_date"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    [formatter setTimeZone:timeZone];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    
    [cell.detailTextLabel setText:[formatter stringFromDate:publishedDate]];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *anArticle = [self.newsArticles objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ViewArticleSegue" sender:anArticle];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ArticleViewController *viewArticleScene = [segue destinationViewController];
    [viewArticleScene setAnArticle:sender];
}


@end
