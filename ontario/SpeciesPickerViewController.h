//
//  SpeciesPickerViewController.h
//  gledn
//
//  Created by Jordan Daniel on 4/3/14.
//  Copyright (c) 2014 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SpeciesPickerViewController;

@protocol SpeciesPickerDelegate <NSObject>

@required
-(void)speciesPickerViewController:(SpeciesPickerViewController *)speciesPicker finishedPickingSpecies:(NSDictionary *)pickedSpecies;
@end


@interface SpeciesPickerViewController : UITableViewController
@property id<SpeciesPickerDelegate> delegate;
@property NSDictionary *speciesList;
@property NSMutableDictionary *speciesData;
@property NSArray *sections;

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;

@end
