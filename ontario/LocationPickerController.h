//
//  LocationPickerController.h
//  Squeal on Pigs
//
//  Created by Jordan Daniel on 5/15/13.
//  Copyright (c) 2013 Jordan Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@class LocationPickerController;

@protocol LocationPickerDelegate <NSObject>

@required
-(void)locationPickerController:(LocationPickerController *)locPicker finishedPickingLocation:(NSDictionary *)pickedLoc;
@end


@interface LocationPickerController : UIViewController <GMSMapViewDelegate>

@property (strong, nonatomic) NSMutableDictionary *pickedLocation;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *pinLatLong;
@property (strong, nonatomic)id <LocationPickerDelegate> delegate;
@property (strong, nonatomic) CLLocation *current;
@property (weak, nonatomic) IBOutlet UIToolbar *polygonToolBar;
@property (strong, nonatomic)NSString *reportEncodedPath;
@property (weak, nonatomic) IBOutlet UISegmentedControl *polygonDrawByControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *pointPolyControl;

- (IBAction)pointPolygonSegmentControlDidChange:(id)sender;
- (IBAction)drawBySegmentControlDidChange:(id)sender;
- (IBAction)handleHelpTap:(id)sender;
- (IBAction)donePressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;
- (IBAction)clearPressed:(id)sender;
- (IBAction)drawPressed:(id)sender;

@end

