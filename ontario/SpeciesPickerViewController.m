//
//  SpeciesPickerViewController.m
//  gledn
//
//  Created by Jordan Daniel on 4/3/14.
//  Copyright (c) 2014 com.bugwood. All rights reserved.
//

#import "SpeciesPickerViewController.h"
#import "AppDelegate.h"

@interface SpeciesPickerViewController ()

@end

@implementation SpeciesPickerViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!self.speciesData) {
        self.speciesData = [[NSMutableDictionary alloc]initWithCapacity:5];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.speciesList objectForKey:[self.sections objectAtIndex:section]] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.sections objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SpeciesCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSDictionary *aSpecies = [[self.speciesList objectForKey:[self.sections objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    [[cell textLabel] setText:[aSpecies valueForKey:@"commonNames"]];
    [[cell detailTextLabel] setText:[aSpecies valueForKey:@"sciname"]];
    
    if ([self.speciesData valueForKey:[[aSpecies valueForKey:@"subid"]stringValue]]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [cell setSelected:YES];
    }else{
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelected:NO];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Cell selected");
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        
        NSDictionary *speciesToAdd = [[self.speciesList valueForKey:[self.sections objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
        
        NSDictionary *objectToAdd = [[NSDictionary alloc]initWithObjects:@[[speciesToAdd valueForKey:@"commonNames"], [speciesToAdd valueForKey:@"sciname"]] forKeys:@[@"comName", @"sciName"]];
        NSLog(@"\nkey - %@\ncomName - %@\nsciName - %@",[[speciesToAdd valueForKey:@"subid"]stringValue],[speciesToAdd valueForKey:@"commonNames"], [speciesToAdd valueForKey:@"sciname"]);
        [self.speciesData setObject:objectToAdd forKey:[[speciesToAdd valueForKey:@"subid"]stringValue]];
    }else if (cell.accessoryType == UITableViewCellAccessoryCheckmark){
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        NSDictionary *speciesToAdd = [[self.speciesList valueForKey:[self.sections objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
        [self.speciesData removeObjectForKey:[[speciesToAdd valueForKey:@"subid"]stringValue]];
    }

    
//    NSLog(@"number of species - %i",[[self.speciesData allKeys] count]);
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Cell deselected");
    
//    NSLog(@"number of species - %i",[[self.speciesData allKeys] count]);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)doneButtonPressed:(id)sender {
    [self.delegate speciesPickerViewController:self finishedPickingSpecies:self.speciesData];
}
@end
