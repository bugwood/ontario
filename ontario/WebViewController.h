//
//  WebViewController.h
//  mrwc
//
//  Created by Jordan Daniel on 10/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *articleSite;
@property (strong, nonatomic) NSString *detailsHTML;
@property (strong, nonatomic) NSString *pdfType;
@property (strong, nonatomic) NSString *aboutPage;

@end
