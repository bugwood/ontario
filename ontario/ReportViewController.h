//
//  ReportViewController.h
//  eddmapswest
//
//  Created by Jordan Daniel on 10/7/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Report.h"
#import <CoreLocation/CoreLocation.h>
#import "LocationPickerController.h"
#import "QueueViewController.h"
#import "AttributesPickerViewController.h"


@interface ReportViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate, LocationPickerDelegate, UIPopoverControllerDelegate, AttributesPickerDelegate>

@property (strong, nonatomic)QueueViewController *queueList;
@property (strong, nonatomic)UIPopoverController *iPadPopoverView;
@property (strong, nonatomic)Report *aReport;
//@property (strong, nonatomic)CLLocation *currentLoc;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addToQueue;

- (IBAction)shootPic:(id)sender;
- (IBAction)pickPic:(id)sender;
- (IBAction)finishedEditing:(id)sender;
- (void)saveReport;
- (IBAction)pickLocation:(id)sender;
- (IBAction)finishReport:(id)sender;
- (void)updatedLocation:(NSNotification*)notif;

@end
