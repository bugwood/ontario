//
//  AppDelegate.h
//  ontario
//
//  Created by Jordan Daniel on 10/15/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "QueueViewController.h"
#import "BWDataFileHandler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CLLocationManager *locManager;
@property (strong, nonatomic) CLLocation *currentloc;
@property (strong, nonatomic) QueueViewController *queueList;
@property (strong, nonatomic) UISplitViewController *splitViewController;
@property (strong, nonatomic) BWDataFileHandler *fileHandler;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(NSInteger)numberOfReports;

@end
