//
//  WebViewController.m
//  mrwc
//
//  Created by Jordan Daniel on 10/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (strong, nonatomic)UIActivityIndicatorView *activityIndicator;
@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityIndicator setFrame:CGRectMake(0, 0, 150, 150)];
    [self.activityIndicator setBackgroundColor:[UIColor grayColor]];
    self.activityIndicator.layer.cornerRadius = 10.0f;
    [self.activityIndicator setAlpha:.6];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator setCenter:CGPointMake(160, 200)];
    [self.view addSubview:self.activityIndicator];

    
    [self.webView setDelegate:self];
    if (self.detailsHTML) {
        [self.webView loadHTMLString:self.detailsHTML baseURL:nil];
    }else if (self.articleSite){
        NSLog(@"article url - %@",self.articleSite);
        NSURL *url = [NSURL URLWithString:self.articleSite];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.webView setDelegate:self];
        [self.webView loadRequest:request];
    }else{
        NSURL *url = [NSURL URLWithString:@"https://apps.bugwood.org/mobile/index.cfm"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.webView loadRequest:request];
    }
    
    
    

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WebView Delegate Methods
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.activityIndicator startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [webView sizeToFit];
    
    for (NSLayoutConstraint *constraint in [webView constraints]) {
        [constraint setConstant:webView.scrollView.frame.size.height];
    }
    
    [self.activityIndicator stopAnimating];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Webview Error" message:[NSString stringWithFormat:@"Sorry but the web view was unable to load - %@", error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
