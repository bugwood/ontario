//
//  DashboardViewController.m
//  njstriketeam
//
//  Created by Jordan Daniel on 9/4/15.
//  Copyright (c) 2015 Jordan Daniel. All rights reserved.
//

#import "DashboardViewController.h"
#import "ObjectsViewController.h"
#import "SpeciesListViewController.h"
#import "AppDelegate.h"
#import <sys/utsname.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NewsFeedViewController.h"

@interface DashboardViewController () <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *reportsLabel;

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([(AppDelegate*)[[UIApplication sharedApplication]delegate] numberOfReports]>0) {
        [self.reportsLabel setHidden:NO];
        [self.reportsLabel setText:[NSString stringWithFormat:@"%ld",(long)[(AppDelegate*)[[UIApplication sharedApplication]delegate]numberOfReports]]];
//                    [viewForBadge sizeToFit];
    }else{
        [self.reportsLabel setHidden:YES];
    }
    self.reportsLabel.clipsToBounds = YES;
    self.reportsLabel.layer.cornerRadius = 5.0f;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(presentNewsFeed:)
                                                 name:@"HAS_PUSH_NOTIFICATION"
                                               object:nil];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)presentNewsFeed:(NSDictionary*)anArticle{
    NSLog(@"presenting article \n%@",anArticle);
    [self performSegueWithIdentifier:@"NewsFeedSegue" sender:anArticle];
}

#pragma mark - Table view data source

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 8) {
        [self actionEmailComposer];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CategoriesSegue"]) {
        ObjectsViewController *groupsView = segue.destinationViewController;
        groupsView.groups = [[(AppDelegate*)[[UIApplication sharedApplication]delegate] fileHandler] GetGroupsFromDataFile];
    }else if ([segue.identifier isEqualToString:@"AllSpeciesSegue"]){
        SpeciesListViewController *allSpeciesView = segue.destinationViewController;
        [allSpeciesView.navigationItem setTitle:@"All Species"];
        
        [allSpeciesView.searchBar setShowsScopeBar:NO];
        [allSpeciesView.searchBar sizeToFit];
        
        NSSortDescriptor *byCommon = [NSSortDescriptor sortDescriptorWithKey:@"commonNames" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        allSpeciesView.selectedGroup = [[[(AppDelegate*)[[UIApplication sharedApplication]delegate] fileHandler] GetAllSpecies] sortedArrayUsingDescriptors:@[byCommon]];
        
    }else if ([segue.identifier isEqualToString:@"MyListSegue"]){
        SpeciesListViewController *myListView = segue.destinationViewController;
        NSArray *mySpeciesList = [[NSUserDefaults standardUserDefaults]objectForKey:@"MySpeciesList"];
        myListView.selectedGroup = [[(AppDelegate*)[[UIApplication sharedApplication]delegate] fileHandler] GetSpecificSpecies:mySpeciesList];
        
        
    }else if ([segue.identifier isEqualToString:@"NegDataSegue"]){
        
    }else if ([segue.identifier isEqualToString:@"UploadQueueSegue"]){
        
    }else if ([segue.identifier isEqualToString:@"LoginSegue"]){
        
    }else if ([segue.identifier isEqualToString:@"MoreSegue"]){
        
    }else if ([segue.identifier isEqualToString:@"NewsFeedSegue"]){
                NewsFeedViewController *newsFeed = segue.destinationViewController;
                if([sender isKindOfClass:[NSDictionary class]]){
                    [newsFeed setRemoteNotif:sender];
                }else{
                    [newsFeed setRemoteNotif:nil];
                }
    }else if ([segue.identifier isEqualToString:@"EditSitesSegue"]) {
//        EditSitesController *editSites = segue.destinationViewController;
//        
//        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//        NSFetchRequest *getSites = [NSFetchRequest fetchRequestWithEntityName:@"Site"];
//        NSSortDescriptor *byName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
//        [getSites setSortDescriptors:@[byName]];
//        NSMutableArray *sites = [NSMutableArray arrayWithArray:[appdelegate.managedObjectContext executeFetchRequest:getSites error:nil]];
//        
//        [editSites setAllSites:sites];
        
    }
}

- (IBAction)actionEmailComposer{
    
    if ([MFMailComposeViewController canSendMail]) {
        struct utsname systemInfo;
        uname(&systemInfo);
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setMailComposeDelegate:self];
        [mailViewController setToRecipients:@[@"eddmaps@ofah.org",@"cbargero@uga.edu",@"tjdaniel@uga.edu"]];
        [mailViewController setSubject:@"EDDMapS Ontario Feedback"];
        NSMutableString *body = [[NSMutableString alloc]init];
        [body appendString:[[NSString alloc]initWithFormat:@"\n\n\nDevice: %@\n", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]]];
        [body appendString:[[NSString alloc]initWithFormat:@"iOS Version: %@\n",[[UIDevice currentDevice]systemVersion]]];
        [body appendString:[[NSString alloc]initWithString:[NSString stringWithFormat:@"App Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]]];
        [mailViewController setMessageBody:body isHTML:NO];
        [self presentViewController:mailViewController animated:YES completion:NULL];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Unable to Send a Message" message:@"Sorry but the app is up able to send email. Be sure you have an activate email account on in your settings and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"Device is unable to send email in its current state.");
    }
    
}

#pragma mark - Mail Compose View delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
