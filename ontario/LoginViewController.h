//
//  LoginViewController.h
//  outSmart
//
//  Created by Jordan Daniel on 6/28/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UIViewController 


@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (nonatomic)BOOL changingUser;

- (IBAction)loginUser:(id)sender;
- (IBAction)loginGuest:(id)sender;
- (IBAction)finsihedEditing:(id)sender;
- (IBAction)forgotStuff:(id)sender;
- (IBAction)selfRegister:(id)sender;
- (IBAction)logout:(id)sender;



@end
