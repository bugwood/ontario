//
//  ArticleViewController.m
//  mrwc
//
//  Created by Jordan Daniel on 12/9/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import "ArticleViewController.h"
#import "WebViewController.h"

@interface ArticleViewController ()

@end

@implementation ArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.titleLabel setText:[self.anArticle objectForKey:@"title"]];
    
    NSDateFormatter *informat = [[NSDateFormatter alloc]init];
    [informat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *publishedDate = [informat dateFromString:[self.anArticle objectForKey:@"published_date"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    [formatter setTimeZone:timeZone];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    [self.publishDate setText:[formatter stringFromDate:publishedDate]];
    
    [self.articleDecript loadHTMLString:[self.anArticle objectForKey:@"description"] baseURL:nil];
    
    [self.articleSource setText:[self.anArticle objectForKey:@"source"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)viewFullArticlePressed:(id)sender {
    [self performSegueWithIdentifier:@"ViewFullArticleSegue" sender:[self.anArticle objectForKey:@"link"]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ViewFullArticleSegue"]) {
        WebViewController *aWebView = segue.destinationViewController;
        [aWebView setArticleSite:sender];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Web View delegate method
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView sizeToFit];
    
    for (NSLayoutConstraint *constraint in [webView constraints]) {
        [constraint setConstant:webView.scrollView.frame.size.height];
    }
    //    [self.mgmtHeight setConstant:self.managementwv.scrollView.frame.size.height];
    
}


@end
