//
//  SightingsController.h
//  gledn
//
//  Created by Jordan Daniel on 7/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SightingsController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *sightingsMap;
@property (strong, nonatomic) NSNumber *speciSubid;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *browserBackItem;

@end
