//
//  LoginViewController.m
//  outSmart
//
//  Created by Jordan Daniel on 6/28/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "AppDelegate.h"

@interface LoginViewController ()
@property (strong , nonatomic)UIActivityIndicatorView *activityIndicator;
@property (strong , nonatomic)AppDelegate *appDelegate;
@end

@implementation LoginViewController

static NSString * const kBaseURL = @"http://www.eddmaps.org/phone/";
static NSString * const kLoginURL = @"auth_post2.cfm?";



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.logoutButton setHidden:YES];
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityIndicator setFrame:CGRectMake(0, 0, 150, 150)];
    [self.activityIndicator setBackgroundColor:[UIColor grayColor]];
    self.activityIndicator.layer.cornerRadius = 10.0f;
    [self.activityIndicator setAlpha:.6];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator setCenter:self.view.center];
    [self.view addSubview:self.activityIndicator];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"]) {
        self.changingUser = YES;
        [self.logoutButton setHidden:NO];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"]) {
        [self.username setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"]];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    if ([self.activityIndicator isAnimating]) {
        [self.activityIndicator stopAnimating];
    }
}

- (IBAction)finsihedEditing:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)forgotStuff:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.eddmaps.org/phone/forget.cfm"]];
}

- (IBAction)selfRegister:(id)sender {
    [self performSegueWithIdentifier:@"RegisterSegue" sender:nil];
}

- (IBAction)logout:(id)sender {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.username setText:@""];
    [self.password setText:@""];
}

- (IBAction)cancelChange:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)loginUser:(id)sender {
    
    [self.activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:self.username.text forKey:@"username"];
    [params setObject:self.password.text forKey:@"password"];
    
    
    AFHTTPRequestOperationManager *httpClient = [AFHTTPRequestOperationManager manager];
    [httpClient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [httpClient POST:@"http://www.eddmaps.org/phone/auth_post2.cfm" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        formData = nil;
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Request Successful, response '%@'", responseStr);
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"loggedIn"];
        [[NSUserDefaults standardUserDefaults] setObject:self.username.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:responseStr forKey:@"userid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login Successful" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.activityIndicator stopAnimating];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        if (operation.response.statusCode == 401) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid Username/Password" message:@"Please check your Username/Password information" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error Sending Information" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        [self.activityIndicator stopAnimating];
    }
     ];
}

- (IBAction)loginGuest:(id)sender {
    [self.activityIndicator startAnimating];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] setObject:@"Guest" forKey:@"username"];
       
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Guest Warning" message:@"As a guest, you will have full utilization of the app with the exception of uploading. If you need to create an account, please visit \"http://www.eddmaps.org\"." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIAlertView delegates methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([[alertView title]isEqualToString:@"Login Successful"]) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        if (self.changingUser) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSLog(@"login successful should dismiss");
            [self dismissViewControllerAnimated:YES completion:NULL];
        }
    }else if([[alertView title]isEqualToString:@"Guest Warning"]){
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        if (self.changingUser) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self dismissViewControllerAnimated:YES completion:NULL];
            self.changingUser = YES;
        }
        
    }
}


@end
