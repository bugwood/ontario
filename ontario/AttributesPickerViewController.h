//
//  AttributesPickerViewController.h
//  hthc
//
//  Created by Jordan Daniel on 10/27/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AttributesPickerViewController;

@protocol AttributesPickerDelegate <NSObject>

@required
-(void)AttributesViewController:(AttributesPickerViewController *)attributesPicker finishedPickingAttributes:(NSArray *)pickedAttributes;

@optional
-(void)canceledPickingAttributes:(AttributesPickerViewController *)attributesPicker;

@end


@interface AttributesPickerViewController : UITableViewController
@property id<AttributesPickerDelegate> delegate;
@property (nonatomic) BOOL multipuleSelection;
@property (strong, nonatomic) NSArray *attributues;
@property (strong, nonatomic) NSMutableArray *selectedAttributes;
@end
