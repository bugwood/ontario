//
//  Report.m
//  ontario
//
//  Created by Jordan Daniel on 9/16/15.
//  Copyright © 2015 com.bugwood. All rights reserved.
//

#import "Report.h"

@implementation Report

// Insert code here to add functionality to your managed object subclass
-(void)addSpeciDescription:(NSString*)newDescription{
    if (!self.speciesDescriptions) {
        self.speciesDescriptions = [[NSMutableArray alloc]init];
    }
    [self.speciesDescriptions addObject:newDescription];
}

-(void)removeSpeciDescription:(NSString*)toRemove{
    [self.speciesDescriptions removeObject:toRemove];
}


@end
