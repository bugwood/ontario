//
//  Report.h
//  ontario
//
//  Created by Jordan Daniel on 9/16/15.
//  Copyright © 2015 com.bugwood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Report : NSManagedObject

typedef NS_ENUM(NSInteger, ReportType)
{
    kReportTypeAquaticPlant = 1,
    kReportTypeAquaticAnimal,
    kReportTypeTerrestialPlant,
    kReportTypeForestPest
};

// Insert code here to declare functionality of your managed object subclass
-(void)addSpeciDescription:(NSString*)newDescription;
-(void)removeSpeciDescription:(NSString*)toRemove;

@end

NS_ASSUME_NONNULL_END

#import "Report+CoreDataProperties.h"
