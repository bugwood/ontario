//
//  ObjectsViewController.m
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 5/31/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "ObjectsViewController.h"
#import "AppDelegate.h"
#import "BWDataFileHandler.h"
#import "SpeciesListViewController.h"

@interface ObjectsViewController ()
@property (strong, nonatomic)UIActivityIndicatorView *activityIndicator;
@end

@implementation ObjectsViewController

@synthesize groups;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion] floatValue] < 7) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ViewHabitSegue"]) {
        SpeciesListViewController *speciesListController = [segue destinationViewController];
        [speciesListController setTitle:sender];
        NSSortDescriptor *byCommon = [NSSortDescriptor sortDescriptorWithKey:@"commonNames" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [speciesListController setSelectedGroup:[(NSArray*)[[(AppDelegate*)[[UIApplication sharedApplication]delegate] fileHandler] GetSpeciesForGroup:sender]sortedArrayUsingDescriptors:@[byCommon]]];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.groups count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"GroupCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.textLabel.text = [self.groups objectAtIndex:[indexPath row]];
    [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[cell.textLabel.text lowercaseString]]]];

    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *selectedSpecies = [self.groups objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ViewHabitSegue" sender:selectedSpecies];
}

@end
