//
//  QueueViewController.m
//  srsfieldguide
//
//  Created by Jordan Daniel on 6/20/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "QueueViewController.h"
#import "AppDelegate.h"
#import "Report.h"
#import "ReportViewController.h"
#import "AFNetworking.h"

@interface QueueViewController ()
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation QueueViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion] floatValue] < 7) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityIndicator setFrame:CGRectMake(0, 0, 150, 150)];
    [self.activityIndicator setBackgroundColor:[UIColor grayColor]];
    self.activityIndicator.layer.cornerRadius = 10.0f;
    [self.activityIndicator setAlpha:.6];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator setCenter:self.view.center];
    [self.view addSubview:self.activityIndicator];
    
    NSError *error;
    NSLog(@"Fetching reports");
    if(![self.fetchedResultsController performFetch:&error]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Sorry but there was an error gathering information." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }

}

//-(void)viewDidAppear:(BOOL)animated{
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ReportViewController *reportReview = segue.destinationViewController;
    [reportReview setAReport:sender];
    [reportReview setQueueList:self];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *nav = [[(UINavigationController*)appDelegate.splitViewController viewControllers]objectAtIndex:1];
        [nav popToRootViewControllerAnimated:NO];
    }
}

- (IBAction)uploadButton{
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"username"] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"username"]isEqualToString:@"Guest"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unauthorize Access" message:@"You must be logged in with an EDDMapS account to upload reports." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }else{
    
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            UINavigationController *nav = [[(UINavigationController*)appDelegate.splitViewController viewControllers]objectAtIndex:1];
            [nav popToRootViewControllerAnimated:NO];
        }
        
        NSArray *fetchedArray = [self.fetchedResultsController fetchedObjects];

        NSMutableDictionary *observationData = [[NSMutableDictionary alloc]initWithCapacity:26];
        NSDictionary *submissionValues = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"SubmissionValues" ofType:@"plist"]];
        
        NSString *userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        NSString *userPW = [[NSUserDefaults standardUserDefaults] stringForKey:@"password"];

        AFHTTPRequestOperationManager *httpClient = [AFHTTPRequestOperationManager manager];
        [httpClient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
        [httpClient.operationQueue setMaxConcurrentOperationCount:3];
        [self.fetchedResultsController.delegate controllerWillChangeContent:self.fetchedResultsController];
        
        for (Report *i in fetchedArray){

            self.view.userInteractionEnabled = FALSE;
            self.navigationItem.leftBarButtonItem.enabled = FALSE;
            self.navigationItem.rightBarButtonItem.enabled = FALSE;
            [self.activityIndicator startAnimating];

            [observationData setObject:userID forKey:@"username"];
            [observationData setObject:userPW forKey:@"password"];
            [observationData setObject:i.dateTime forKey:@"datetime"];
            [observationData setObject:[i subID] forKey:@"subid"];
            [observationData setObject:i.name forKey:@"science"];
            [observationData setObject:i.lat forKey:@"latitude" ];
            [observationData setObject:i.lng forKey:@"longitude"];
            [observationData setObject:@"39" forKey:@"project"];
            [observationData setObject:[i accuracy] forKey:@"accuracy"];
            [observationData setObject:[i course] forKey:@"course"];
            [observationData setObject:[i speed] forKey:@"speed"];
            [observationData setObject:[i elavation] forKey:@"elavation"];
            [observationData setObject:([i notes]&&![i.notes isEqualToString:@"Additional information goes here. Do not include personal information. All information entered in this field will be displayed publicly."]?[i notes]:@"") forKey:@"notes"];
            [observationData setObject:([i abundance]?[submissionValues objectForKey:[i abundance]]:@"") forKey:@"abundance"];
            [observationData setObject:([i amount]?[i amount]:@"") forKey:@"amount"];
//            [observationData setObject:[i animal] forKey:@"flag"];
            [observationData setObject:([i area]?[i area]:@"") forKey:@"area"];
            [observationData setObject:([i areaType]?[i areaType]:@"") forKey:@"areaType"];
            [observationData setObject:([i coverage]?[submissionValues objectForKey:[i coverage]]:@"") forKey:@"coverage"];
            [observationData setObject:([i dectMethod]?[i dectMethod]:@"") forKey:@"detectionMethod"];
            [observationData setObject:([i habitat]?[submissionValues objectForKey:[i habitat]]:@"")  forKey:@"habitat"];
            if ([[i reportType]integerValue]==kReportTypeAquaticAnimal) {
                if ([i speciesDescriptions]!=nil) {
                    NSArray *descriptions = [NSArray arrayWithArray:[[i speciesDescriptions]componentsSeparatedByString:@"|"]];
                    [observationData setObject:[descriptions objectAtIndex:0] forKey:@"lifestatus"];
                    [observationData setObject:[descriptions objectAtIndex:1] forKey:@"descriptions"];
                }else{
                    [observationData setObject:@"" forKey:@"lifestatus"];
                    [observationData setObject:@"" forKey:@"descriptions"];
                }
                [observationData setObject:([i largest]?[i largest]:@"") forKey:@"largest"];
                [observationData setObject:([i largestType]?[submissionValues objectForKey:[i largestType]]:@"") forKey:@"largestType"];
                [observationData setObject:([i smallest]?[i smallest]:@"") forKey:@"smallest"];
                [observationData setObject:([i smallestType]?[submissionValues objectForKey:[i smallestType]]:@"") forKey:@"smallestType"];
                
            }else{
                NSString *descriptions = [[NSString alloc]init];
                if ([[i speciesDescriptions]isKindOfClass:[NSArray class]]) {
                    if ([[i speciesDescriptions]count]>0) {
                        descriptions = [[i speciesDescriptions]objectAtIndex:0];
                        for (int j = 1; j<[[i speciesDescriptions]count]; j++) {
                            descriptions = [NSString stringWithFormat:@"%@,%@", descriptions, [[i speciesDescriptions]objectAtIndex:j]];
                        }

                    }else{
                        descriptions = @"";
                    }
                }else{
                    descriptions = [i speciesDescriptions]?[i speciesDescriptions]:@"";
                }
                [observationData setObject:descriptions forKey:@"descriptions"];
                [observationData setObject:@"" forKey:@"largest"];
                [observationData setObject:@"" forKey:@"largestType"];
                [observationData setObject:@"" forKey:@"smallest"];
                [observationData setObject:@"" forKey:@"smallestType"];
            }
            
            
            
            [httpClient POST:@"http://www.eddmaps.org/phone/upload.cfm" parameters:observationData constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                if ([i isKindOfClass:[Report class]]&&[i valueForKey:@"imageData"]!=nil) {
                    NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithData:[i valueForKey:@"imageData"]], .80);
                    [formData appendPartWithFileData:imageData name:@"file1" fileName:@"temp.jpg" mimeType:@"image/jpeg"];
                }else{
                    formData = nil;
                }
            } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"response string - '%@'",responseStr);
                if ([responseStr isEqualToString:@"UPLOADED_OK"]) {
                    [self.fetchedResultsController.delegate controller:self.fetchedResultsController
                                                       didChangeObject:i
                                                           atIndexPath:[self.fetchedResultsController indexPathForObject:i]
                                                         forChangeType:NSFetchedResultsChangeDelete
                                                          newIndexPath:nil];
                    
                    
                }
                if ([[httpClient.operationQueue operations]count] == 0) {
                    [self.activityIndicator stopAnimating];
                    self.navigationItem.leftBarButtonItem.enabled = TRUE;
                    self.navigationItem.rightBarButtonItem.enabled = TRUE;
                    self.view.userInteractionEnabled = TRUE;
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Upload Finished" message:@"The Upload Proccess has Finished. If any reports did not successfully complete, they were not removed from the queue to try again later. \nThank you" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    [self.fetchedResultsController.delegate controllerDidChangeContent:self.fetchedResultsController];
                    
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"on subid - %@, Error: %@", [observationData valueForKey:@"subid"],error);
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error Sending repport" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                if ([[httpClient.operationQueue operations]count] == 0) {
                    [self.activityIndicator stopAnimating];
                    self.navigationItem.leftBarButtonItem.enabled = TRUE;
                    self.navigationItem.rightBarButtonItem.enabled = TRUE;
                    self.view.userInteractionEnabled = TRUE;
                    [self.fetchedResultsController.delegate controllerDidChangeContent:self.fetchedResultsController];
                }
                
            }];
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.fetchedResultsController fetchedObjects]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ReportCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Report *aReport = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell.textLabel setText:[aReport name]];
    [cell.detailTextLabel setText:[aReport commonName]];
//    [cell.imageView setImage:[UIImage imageWithData:[aReport imageData]]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
		NSManagedObjectContext *context = [_fetchedResultsController managedObjectContext];
		[context deleteObject:[_fetchedResultsController objectAtIndexPath:indexPath]];
		
		// Save the context.
		NSError *error;
		if (![context save:&error]) {
			/*
			 Replace this implementation with code to handle the error appropriately.
			 
			 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
			 */
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Report *selectedReport = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ViewReportSegue" sender:selectedReport];
    

}

#pragma mark - FetchedResultsController Property

- (NSFetchedResultsController *)fetchedResultsController {
    if(_fetchedResultsController != nil){
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Report" inManagedObjectContext:managedObjectContext];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
	// Edit the sort key as appropriate.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	// Edit the section name key path and cache name if appropriate.
	// nil for section name key path means "no sections".
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:@"Reports"];
    _fetchedResultsController.delegate = self;

	
	return _fetchedResultsController;
}


/**
 Delegate methods of NSFetchedResultsController to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	UITableView *tableView = self.tableView;
   	
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
            break;
			
		case NSFetchedResultsChangeMove:
//			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//          [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
	}
    
}


//- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
//	switch(type) {
//		case NSFetchedResultsChangeInsert:
//			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
//			break;
//			
//		case NSFetchedResultsChangeDelete:
//			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
//			break;
//	}
//    
//}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}


@end
