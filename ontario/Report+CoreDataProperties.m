//
//  Report+CoreDataProperties.m
//  ontario
//
//  Created by Jordan Daniel on 10/5/15.
//  Copyright © 2015 com.bugwood. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Report+CoreDataProperties.h"

@implementation Report (CoreDataProperties)

@dynamic abundance;
@dynamic accuracy;
@dynamic amount;
@dynamic area;
@dynamic areaType;
@dynamic commonName;
@dynamic course;
@dynamic coverage;
@dynamic dateTime;
@dynamic dectMethod;
@dynamic elavation;
@dynamic habitat;
@dynamic images;
@dynamic largest;
@dynamic largestType;
@dynamic lat;
@dynamic lng;
@dynamic name;
@dynamic notes;
@dynamic protocol;
@dynamic reportType;
@dynamic smallest;
@dynamic smallestType;
@dynamic speciesDescriptions;
@dynamic speed;
@dynamic subID;
@dynamic treeHost;

@end
