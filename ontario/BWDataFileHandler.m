//
//  BWDataFileHandler.m
//  njstriketeam
//
//  Created by Jordan Daniel on 2/27/14.
//  Copyright (c) 2014 Jordan Daniel. All rights reserved.
//

#import "BWDataFileHandler.h"
#import "AFNetworking.h"
//#import "Site.h"
#import "AppDelegate.h"

@interface BWDataFileHandler ()

@property NSDictionary *dataFile;
@property NSTimer *currentSiteTimer;

@end

@implementation BWDataFileHandler

- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"Intializing data file handler");
        NSError *err;
        
        NSData *datafile = [NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ontario-ios" ofType:@"json"]];
        
        self.dataFile = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:datafile options:kNilOptions error:&err];
//        
//        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//        NSFetchRequest *getSites = [NSFetchRequest fetchRequestWithEntityName:@"Site"];
//        NSPredicate *currentSitePredicate = [NSPredicate predicateWithFormat:@"currentSite == YES"];
//        [getSites setPredicate:currentSitePredicate];
        
//        NSArray *sites = [appdelegate.managedObjectContext executeFetchRequest:getSites error:nil];
        
//        if (sites.count > 0) {
//            self.currentSite = [sites objectAtIndex:0];
//        }

        
        if (err) {
            NSLog(@"%@", err.localizedDescription);
        }
        
    }
    return self;
}

-(NSArray*)GetGroupsFromDataFile{
   return (NSArray*)[[[self.dataFile objectForKey:@"subjects"] valueForKeyPath:@"@distinctUnionOfObjects.taxonGroup"]sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

-(NSArray*)GetAllSpecies{
    NSArray *allSpecies = (NSArray*)[self.dataFile objectForKey:@"subjects"];
    return allSpecies;
}

-(NSArray *)GetSpecificSpecies:(NSArray *)speciesids{
    NSArray *specificSpecies = [[NSArray alloc]init];
    NSMutableArray *specificSpeciesPredicates = [[NSMutableArray alloc]initWithCapacity:[speciesids count]];

    for (int i = 0; i<[speciesids count]; i++) {
        NSPredicate *idPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.subid == %@",speciesids[i]]];
        [specificSpeciesPredicates addObject:idPredicate];
    }
    
    NSPredicate *getSpeciesPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:specificSpeciesPredicates];
    NSArray *allSpecies = (NSArray*)[self.dataFile objectForKey:@"subjects"];
    specificSpecies = [allSpecies filteredArrayUsingPredicate:getSpeciesPredicate];
    
    return specificSpecies;
}

-(NSDictionary*)GetAllSpeciesForPicker{
    NSMutableDictionary *listForPicker = [[NSMutableDictionary alloc]initWithCapacity:12];
    NSArray *groups = [self GetGroupsFromDataFile];
    NSSortDescriptor *byCommon = [NSSortDescriptor sortDescriptorWithKey:@"commonNames" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    for (NSString *aGroup in groups) {
        NSArray *groupToAdd = [[self GetSpeciesForGroup:aGroup]sortedArrayUsingDescriptors:@[byCommon]];
        [listForPicker setObject:groupToAdd forKey:aGroup];
    }
    
    return listForPicker;
}

-(NSArray*)GetSpeciesForGroup:(NSString*)groupName{
    NSArray *allSpecies = [self.dataFile objectForKey:@"subjects"];
    
    NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.taxonGroup == \"%@\"",groupName]];
    NSArray *speciesOfGroup = [NSMutableArray arrayWithArray:[allSpecies filteredArrayUsingPredicate:groupPredicate]];
    return speciesOfGroup;
}

//-(void)storeSites:(EditSitesController *)activeController{
//    
//    AFHTTPRequestOperationManager *httpClient = [AFHTTPRequestOperationManager manager];
//    [httpClient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
//    
//    [httpClient GET:@"http://www.eddmaps.org/phone/getSites.cfm"
//         parameters:@{@"project":@54}
//            success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                NSArray *returnedSites = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
//                NSLog(@"response string - '%@'",returnedSites);
//                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//                for (NSDictionary* aSite in returnedSites) {
//                    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//                    NSFetchRequest *getSites = [NSFetchRequest fetchRequestWithEntityName:@"Site"];
//                    NSSortDescriptor *byName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
//                    [getSites setSortDescriptors:@[byName]];
//                    NSArray *sites = [appdelegate.managedObjectContext executeFetchRequest:getSites error:nil];
//                    NSMutableDictionary *oldSites = [NSMutableDictionary dictionary];
//                    for (Site *oldSite in sites) {
//                        if ([oldSite serverid]>0) {
//                            [oldSites setObject:oldSites forKey:[[oldSite serverid]stringValue]];
//                        }
//                    }
//                    if (![oldSites objectForKey:[[aSite objectForKey:@"id"] stringValue]]) {
//                        Site *aNewSite = [NSEntityDescription insertNewObjectForEntityForName:@"Site" inManagedObjectContext:appDelegate.managedObjectContext];
//                        [aNewSite setServerid:[NSNumber numberWithInteger:[[aSite valueForKey:@"id"] integerValue]]];
//                        [aNewSite setName:[aSite objectForKey:@"sitename"]];
//                        [appDelegate.managedObjectContext save:nil];
//                    }
//                    if (activeController) {
//                        [activeController setAllSites:[NSMutableArray arrayWithArray:[appdelegate.managedObjectContext executeFetchRequest:getSites error:nil]]];
//                        [activeController.tableView reloadData];
//                        [activeController.activityIndicator stopAnimating];
//                    }
//                }
//            }
//            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"Error: %@",error);
//            }
//     ];
//}
//
//-(void)createRecentSiteTimer:(Site *)mostRecentSite{
//    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    if (!self.currentSite) {
//        NSLog(@"setting recent site to most recent site");
//        [mostRecentSite setCurrentSite:[NSNumber numberWithBool:YES]];
//        [appDelegate.managedObjectContext save:nil];
//        self.currentSite = mostRecentSite;
//        self.currentSiteTimer = [NSTimer timerWithTimeInterval:14400
//                                                       target:self
//                                                     selector:@selector(clearRecentSite)
//                                                     userInfo:nil
//                                                      repeats:NO];
//        [[NSRunLoop currentRunLoop]addTimer:self.currentSiteTimer forMode:NSDefaultRunLoopMode];
//    }else{
//        NSLog(@"compairing recentSite to mostRecentSite");
//        if (![mostRecentSite.name isEqualToString:self.currentSite.name]) {
//            NSLog(@"New Recent Site Stored");
//            [self.currentSite setCurrentSite:[NSNumber numberWithBool:NO]];
//            [mostRecentSite setCurrentSite:[NSNumber numberWithBool:YES]];
//            [appDelegate.managedObjectContext save:nil];
//            self.currentSite = mostRecentSite;
//            
//            [self.currentSiteTimer invalidate];
//            
//            self.currentSiteTimer = [NSTimer timerWithTimeInterval:14400
//                                                           target:self
//                                                         selector:@selector(clearRecentSite)
//                                                         userInfo:nil
//                                                          repeats:NO];
//            [[NSRunLoop currentRunLoop]addTimer:self.currentSiteTimer forMode:NSDefaultRunLoopMode];
//        }
//    }
//}
//
//-(void)clearRecentSite{
//    NSLog(@"removing recent site");
//    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    
//    [self.currentSite setCurrentSite:[NSNumber numberWithBool:NO]];
//    [appDelegate.managedObjectContext save:nil];
//    
//    self.currentSite = nil;
//}

@end
