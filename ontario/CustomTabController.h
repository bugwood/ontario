//
//  CustomTabController.h
//  gledn
//
//  Created by Jordan Daniel on 4/9/14.
//  Copyright (c) 2014 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabController : UITabBarController

@property (strong, nonatomic)NSNumber *subid;

- (IBAction)dismissButtonPressed:(id)sender;
- (IBAction)addToMyListPressed:(id)sender;

@end
