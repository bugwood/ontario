//
//  ObjectsViewController.h
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 5/31/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SpeciesListViewController.h"

@interface ObjectsViewController : UITableViewController 
@property (strong, nonatomic)NSArray *groups;
@end
